(ns tarsonis.main
  (:require
   [om.core :as om :include-macros true]
   [om.dom :as dom :include-macros true]

   [ixinfestor.net :as ixnet]
   [ixinfestor.omut :as omut]
   [ixinfestor.omut-webusers :as webusers]

   [tarsonis.tarsonis-oms :as omut-ts]

   [tarsonis.own-oms :as own]
   [tarsonis.product-oms :as product]

   [tarsonis.op-move :as op-move]
   [tarsonis.op-move-as-form :as op-move-as-form]

   [secretary.core :as sec :include-macros true]
   [goog.events :as events]
   [goog.history.EventType :as EventType])
  (:import goog.History))

(enable-console-print!)


;;**************************************************************************************************
;;* BEGIN setup browser history
;;* tag: <browser history>
;;*
;;* description: Инициализация созранения истории навигации по приложению в браузере
;;*
;;**************************************************************************************************

(sec/set-config! :prefix "#")

(let [history (History.)
      navigation EventType/NAVIGATE]
  (goog.events/listen history
                      navigation
                      #(-> % .-token sec/dispatch!))
  (doto history (.setEnabled true)))


;; END setup browser history
;;..................................................................................................


;;**************************************************************************************************
;;* BEGIN applicaton data state
;;* tag: <app data state>
;;*
;;* description: состояние приложения в виде данных
;;*
;;**************************************************************************************************

;; Декларируем ключи
(def key-opts :opts)
(def key-opt-prod-*  :prod-*)
(def key-opt-money-* :money-*)



;; Состояние приложения и инициализация ее начальной структуры
(defonce app-state
  (atom {omut/nav-app-state-key {:left [;; 0
                                        {:glyphicon "glyphicon-shopping-cart"
                                         :text "Операции"
                                         :sub []} ;; <-<< будут вставлены разрешенные операции

                                        ;; 1
                                        {:glyphicon "glyphicon-book"
                                         :text "Справочники"
                                         :sub [
                                               {:glyphicon "glyphicon-book"
                                                :text "Владельци счетов"
                                                :href "#/rb/owns"}

                                               {:glyphicon "glyphicon-book"
                                                :text "Товары"
                                                :href "#/rb/products"}

                                               ]}

                                        {:glyphicon "glyphicon-cog"
                                         :text "Администрирование"
                                         :sub [
                                               {:glyphicon "glyphicon-user"
                                                :text "Управление пользователями"
                                                :href "#/adm/webusers"}
                                               ]}


                                        ]
                                 :right [
                                         {:glyphicon "glyphicon-user"
                                          :text "Пользователь"
                                          :sub [{:glyphicon "glyphicon-lock"
                                                 :text "Сменить пароль..."
                                                 :href "#/user/change-password"}
                                                {:separator? true}
                                                {:glyphicon "glyphicon-log-out"
                                                 :text "Выход"
                                                 :href "/logout"}]}
                                         ]
                                 }

         key-opts {key-opt-prod-* {}
                   key-opt-money-* {}}

         :rb {:owns own/own-search-view-app-init
              :products product/product-search-view-app-init
              }

         :adm {:webusers webusers/webusers-search-view-app-init}

         :change-password-form webusers/webusers-change-password-form-app-init

         }))

;; Курсоры
(defn cursor-root [] (om/root-cursor app-state))

(defn cursor-menu [] (om/ref-cursor (omut/nav-app-state-key (cursor-root))))
(defn cursor-menu-left  [] (om/ref-cursor (:left (cursor-menu))))
(defn cursor-menu-left-opt-sub [] (om/ref-cursor (get-in (cursor-menu-left) [0 :sub])))

(defn cursor-menu-right [] (om/ref-cursor (:right (cursor-menu))))
(defn cursor-menu-right-user [] (om/ref-cursor ((cursor-menu-right) 0)))

(defn cursor-opts-prod-* [] (om/ref-cursor (get-in (cursor-root) [key-opts key-opt-prod-*])))
(defn cursor-opts-money-* [] (om/ref-cursor (get-in (cursor-root) [key-opts key-opt-money-*])))

;; Справочники
(defn cursor-rb [] (om/ref-cursor (:rb (cursor-root))))
(defn cursor-owns [] (om/ref-cursor (:owns (cursor-rb))))
(defn cursor-products [] (om/ref-cursor (:products (cursor-rb))))


;; Администрирование
(defn cursor-adm [] (om/ref-cursor (:adm (cursor-root))))
(defn cursor-webusers [] (om/ref-cursor (:webusers (cursor-adm))))




;; Наблюдение для отладки
(add-watch
 app-state :log
 (fn [_ _ old new]
   (when (not= old new)
     ;;(println new)
     ;;(println key-opt-prod-* (key-opt-prod-* new))
     )))

;; Выполнение первого запроса на сервер и получение необходимых метаданных
;; и данных по текущему пользователю. Далее в зависимости от данных идет модификация
;; глобального состояни приложения
(ixnet/get-data
 "/tc/opt/meta"
 {}
 (fn [result]
   (println "[META]:" (str result))

   ;; добавляем метаданные
   (om/transact! (cursor-root) #(merge % result))

   ;; строим меню операций
   (om/transact!
    (cursor-menu-left-opt-sub)
    (fn [sub]
      (->> result
           :meta-opt
           vals
           (map (fn [{:keys [opt opttype title]}]
                  {:text title
                   :href (str "#/opt/" (name opttype) "/" (name opt))}))
           vec)))

   ;; Подготавливаем данные для операции перемещения --------------------------------------
   ;; Каждая операция app-init сама выберит то что ей необходимо в свою структуру данных
   (om/update! (cursor-opts-prod-*) (op-move/app-init (result :meta-opt)))
   (om/update! (cursor-opts-money-*) (op-move-as-form/app-init (result :meta-opt)))
   ;; -------------------------------------------------------------------------------------

   ;; Модифицируем меню пользователя
   (om/update! (cursor-menu-right-user) :text
               (-> result :authentication :user :username))

   ))


;; END applicaton data state
;;..................................................................................................

;; Навигационное меню
(om/root omut/nav app-state {:target (. js/document (getElementById "top-nav"))})


(defn root-on-div-main [r-fn]
  (om/root r-fn app-state {:target (. js/document (getElementById "main"))}))


;; Операции перемещения товара
(sec/defroute index-page "/opt/:opttype/:opt" {:keys [opttype opt]}
  (let [opttype (keyword opttype)
        opt     (keyword opt)]
    (println "Переключение на операцию: тип" opttype ", операция" opt)

    (condp contains? opttype

      op-move/opttypes
      (root-on-div-main
       (fn [_ _]
         (reify
           om/IRender
           (render [_]
             (om/build op-move/panel (cursor-opts-prod-*) {key-opts {:opt opt}})))))


      op-move-as-form/opttypes
      (root-on-div-main
       (fn [_ _]
         (reify
           om/IRender
           (render [_]
             (om/build op-move-as-form/panel (cursor-opts-money-*) {key-opts {:opt opt}})))))


      (let [message (str "Отображение операции по типу " opttype " еще не определено!")]
        (println message)
        (root-on-div-main
         (fn [_ _]
           (reify
             om/IRender
             (render [_]
               (dom/h1 #js {:className "text-warning"}
                       message)))))))))



(sec/defroute page-rb-owns "/rb/owns" []
  (root-on-div-main
   (fn [_ _]
     (reify
       om/IRender
       (render [_]
         (om/build own/own-search-view (cursor-owns)
                   {:opts {:editable? true}}))))))


(sec/defroute index-rb-products "/rb/products" []
  (root-on-div-main
   (fn [_ _]
     (reify
       om/IRender
       (render [_]
         (om/build product/product-search-view (cursor-products)
                   {:opts {:editable? true}}))))))


(sec/defroute index-adm-webusers "/adm/webusers" []
  (root-on-div-main
   (fn [_ _]
     (reify
       om/IRender
       (render [_]
         (om/build webusers/webusers-search-view (cursor-webusers)
                   {:opts {:editable? true}}))))))



(sec/defroute index-change-password "/user/change-password" []
  (root-on-div-main
   (fn [_ _]
     (reify
       om/IRender
       (render [_]
         (om/build webusers/webusers-change-password-form (:change-password-form (cursor-root)))

         )))))





(defn main []
  (-> js/document
      .-location
      (set! "#/")))


(main)



;;(op-move/main)
