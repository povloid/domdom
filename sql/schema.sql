--
-- PostgreSQL database dump
--

-- Dumped from database version 9.4.4
-- Dumped by pg_dump version 9.4.4
-- Started on 2015-08-28 17:51:18 ALMT

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- TOC entry 216 (class 3079 OID 11861)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 2341 (class 0 OID 0)
-- Dependencies: 216
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

--
-- TOC entry 229 (class 1255 OID 191342)
-- Name: own_fts_indexation(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION own_fts_indexation() RETURNS trigger
    LANGUAGE plpgsql
    AS 'DECLARE
BEGIN

	
  IF TG_OP = ''INSERT'' OR TG_OP =''UPDATE'' THEN	
	NEW.fts=(
		setweight( coalesce( to_tsvector(''id '' || NEW.id),''''),''A'') || '' '' || 
		setweight( coalesce( to_tsvector(NEW.keyname),''''),''B'') || '' '' || 		
		
		setweight( coalesce( to_tsvector(NEW.description),''''),''D''));
	
  END IF;
  

  RETURN NEW;
END;';


--
-- TOC entry 230 (class 1255 OID 191343)
-- Name: stext_fts_indexation(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION stext_fts_indexation() RETURNS trigger
    LANGUAGE plpgsql
    AS 'DECLARE
BEGIN
	
  IF TG_OP = ''INSERT'' OR TG_OP =''UPDATE'' THEN	
	NEW.fts=(setweight( coalesce( to_tsvector(''id '' || NEW.id),''''),''A'') || '' '' ||
		 setweight( coalesce( to_tsvector(''english'',NEW.keyname),''''),''D'') || '' '' ||		 
		 setweight( coalesce( to_tsvector(''english'',NEW.description),''''),''D'') || '' '' ||		 		 
		 setweight( coalesce( to_tsvector(''english'',NEW.anytext),''''),''D''));
	
  END IF;
  
  RETURN NEW;
END;';


--
-- TOC entry 231 (class 1255 OID 191344)
-- Name: webdoc_fts_indexation(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION webdoc_fts_indexation() RETURNS trigger
    LANGUAGE plpgsql
    AS 'DECLARE
BEGIN

	
  IF TG_OP = ''INSERT'' OR TG_OP =''UPDATE'' THEN	
	NEW.fts=(
		setweight( coalesce( to_tsvector(''id '' || NEW.id),''''),''A'') || '' '' || 
		setweight( coalesce( to_tsvector(NEW.keyname),''''),''B'') || '' '' || 		
		setweight( coalesce( to_tsvector(NEW.scod),''''),''C'') || '' '' ||
		setweight( coalesce( to_tsvector(NEW.description),''''),''D'') || '' '' ||
		
		setweight( coalesce( to_tsvector(NEW.web_title),''''),''D'') || '' '' ||
		setweight( coalesce( to_tsvector(NEW.web_meta_subject),''''),''D'') || '' '' ||
		setweight( coalesce( to_tsvector(NEW.web_meta_keywords),''''),''D'') || '' '' ||
		setweight( coalesce( to_tsvector(NEW.web_meta_description),''''),''D'') || '' '' ||

		setweight( coalesce( to_tsvector(NEW.web_description),''''),''D'') || '' '' ||
		setweight( coalesce( to_tsvector(NEW.web_top_description),''''),''D''));
	
  END IF;
  

  RETURN NEW;
END;';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 172 (class 1259 OID 191345)
-- Name: acc; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE acc (
    id integer NOT NULL,
    version_id integer NOT NULL,
    description text,
    zone_id integer NOT NULL,
    own_id integer NOT NULL,
    webdoc_id integer NOT NULL,
    ca_root_id integer NOT NULL,
    ca_id integer NOT NULL,
    ca_parent_id integer,
    tip character varying(20) NOT NULL,
    val_bigint bigint,
    val_money numeric(20,2),
    val_numeric numeric(20,3),
    val_ratio_num bigint,
    val_ratio_denum bigint
);


--
-- TOC entry 173 (class 1259 OID 191351)
-- Name: acc_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE acc_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2342 (class 0 OID 0)
-- Dependencies: 173
-- Name: acc_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE acc_id_seq OWNED BY acc.id;


--
-- TOC entry 174 (class 1259 OID 191353)
-- Name: arl; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE arl (
    id integer NOT NULL,
    keyname character varying(50) NOT NULL,
    title character varying(255),
    description text
);


--
-- TOC entry 175 (class 1259 OID 191359)
-- Name: arl_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE arl_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2343 (class 0 OID 0)
-- Dependencies: 175
-- Name: arl_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE arl_id_seq OWNED BY arl.id;


--
-- TOC entry 176 (class 1259 OID 191361)
-- Name: ca; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE ca (
    id integer NOT NULL,
    root_id integer,
    parent_id integer,
    tip character varying(20) NOT NULL,
    keyname character varying(50) NOT NULL,
    title character varying(20) NOT NULL,
    description text
);


--
-- TOC entry 177 (class 1259 OID 191367)
-- Name: ca_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE ca_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2344 (class 0 OID 0)
-- Dependencies: 177
-- Name: ca_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE ca_id_seq OWNED BY ca.id;


--
-- TOC entry 178 (class 1259 OID 191369)
-- Name: files; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE files (
    id integer NOT NULL,
    path character varying(512) NOT NULL,
    filename character varying(255) NOT NULL,
    urlpath text,
    top_description text,
    description text,
    content_type character varying(255),
    size bigint,
    galleria boolean DEFAULT true,
    fts tsvector
);


--
-- TOC entry 179 (class 1259 OID 191376)
-- Name: files_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE files_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2345 (class 0 OID 0)
-- Dependencies: 179
-- Name: files_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE files_id_seq OWNED BY files.id;


--
-- TOC entry 180 (class 1259 OID 191378)
-- Name: files_rel; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE files_rel (
    files_id integer,
    webdoc_id integer
);


--
-- TOC entry 181 (class 1259 OID 191381)
-- Name: op; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE op (
    id integer NOT NULL,
    prev_id integer,
    opttype character varying(50) NOT NULL,
    opt character varying(50) NOT NULL,
    cdate timestamp with time zone NOT NULL,
    webuser_id integer NOT NULL,
    description text,
    CONSTRAINT op_prev_id_ck CHECK ((prev_id <> id))
);


--
-- TOC entry 182 (class 1259 OID 191388)
-- Name: op_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE op_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2346 (class 0 OID 0)
-- Dependencies: 182
-- Name: op_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE op_id_seq OWNED BY op.id;


--
-- TOC entry 183 (class 1259 OID 191390)
-- Name: opi; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE opi (
    id integer NOT NULL,
    op_id integer NOT NULL,
    opttype character varying(50) NOT NULL,
    opt character varying(50) NOT NULL,
    description text,
    acc_id integer NOT NULL,
    zone_id integer NOT NULL,
    own_id integer NOT NULL,
    webdoc_id integer NOT NULL,
    acc_version_id integer NOT NULL,
    ca_root_id integer NOT NULL,
    ca_id integer NOT NULL,
    ca_parent_id integer,
    tip character varying(20) NOT NULL,
    val_bigint bigint,
    val_money numeric(20,2),
    val_numeric numeric(20,3),
    val_ratio_num bigint,
    val_ratio_denum bigint
);


--
-- TOC entry 184 (class 1259 OID 191396)
-- Name: opi_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE opi_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2347 (class 0 OID 0)
-- Dependencies: 184
-- Name: opi_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE opi_id_seq OWNED BY opi.id;


--
-- TOC entry 185 (class 1259 OID 191398)
-- Name: opiacc; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE opiacc (
    opi_id integer NOT NULL,
    id integer NOT NULL,
    version_id integer NOT NULL,
    tip character varying(20) NOT NULL,
    val_bigint bigint,
    val_money numeric(20,2),
    val_numeric numeric(20,3),
    val_ratio_num bigint,
    val_ratio_denum bigint
);


--
-- TOC entry 215 (class 1259 OID 192874)
-- Name: opiattrs; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE opiattrs (
    opi_id integer NOT NULL,
    keyname character varying(20) NOT NULL,
    tip character varying(20) NOT NULL,
    val_bigint bigint,
    val_money numeric(20,2),
    val_numeric numeric(20,3),
    val_ratio_num bigint,
    val_ratio_denum bigint
);


--
-- TOC entry 186 (class 1259 OID 191401)
-- Name: opt; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE opt (
    id integer NOT NULL,
    opttype character varying(50) NOT NULL,
    opt character varying(50) NOT NULL,
    title character varying(255) NOT NULL,
    description text
);


--
-- TOC entry 187 (class 1259 OID 191407)
-- Name: opt_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE opt_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2348 (class 0 OID 0)
-- Dependencies: 187
-- Name: opt_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE opt_id_seq OWNED BY opt.id;


--
-- TOC entry 188 (class 1259 OID 191409)
-- Name: opttype; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE opttype (
    id integer NOT NULL,
    opttype character varying(50) NOT NULL,
    title character varying(255) NOT NULL,
    description text
);


--
-- TOC entry 189 (class 1259 OID 191415)
-- Name: opttype_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE opttype_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2349 (class 0 OID 0)
-- Dependencies: 189
-- Name: opttype_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE opttype_id_seq OWNED BY opttype.id;


--
-- TOC entry 190 (class 1259 OID 191417)
-- Name: own; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE own (
    id integer NOT NULL,
    keyname character varying(50) NOT NULL,
    description text,
    fts tsvector
);


--
-- TOC entry 191 (class 1259 OID 191423)
-- Name: own_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE own_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2350 (class 0 OID 0)
-- Dependencies: 191
-- Name: own_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE own_id_seq OWNED BY own.id;


--
-- TOC entry 192 (class 1259 OID 191425)
-- Name: ownarl; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE ownarl (
    own_id integer NOT NULL,
    arl_id integer NOT NULL
);


--
-- TOC entry 193 (class 1259 OID 191428)
-- Name: stext; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE stext (
    id integer NOT NULL,
    keyname character varying(50) NOT NULL,
    description text,
    plantext boolean DEFAULT false NOT NULL,
    anytext text,
    fts tsvector
);


--
-- TOC entry 194 (class 1259 OID 191435)
-- Name: stext_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE stext_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2351 (class 0 OID 0)
-- Dependencies: 194
-- Name: stext_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE stext_id_seq OWNED BY stext.id;


--
-- TOC entry 195 (class 1259 OID 191437)
-- Name: swop; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE swop (
    id integer NOT NULL,
    swop character varying(20) NOT NULL,
    description text
);


--
-- TOC entry 196 (class 1259 OID 191443)
-- Name: swop_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE swop_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2352 (class 0 OID 0)
-- Dependencies: 196
-- Name: swop_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE swop_id_seq OWNED BY swop.id;


--
-- TOC entry 197 (class 1259 OID 191445)
-- Name: swopv; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE swopv (
    id integer NOT NULL,
    opttype character varying(50) NOT NULL,
    opt character varying(50) NOT NULL,
    swop character varying(20) NOT NULL,
    swebdoc_id integer NOT NULL,
    include boolean
);


--
-- TOC entry 198 (class 1259 OID 191448)
-- Name: swopv_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE swopv_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2353 (class 0 OID 0)
-- Dependencies: 198
-- Name: swopv_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE swopv_id_seq OWNED BY swopv.id;


--
-- TOC entry 199 (class 1259 OID 191450)
-- Name: swopva; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE swopva (
    id integer NOT NULL,
    swopv_id integer NOT NULL,
    keyname character varying(20) NOT NULL,
    webdoc_id integer NOT NULL,
    ca_id integer NOT NULL,
    tip character varying(20) NOT NULL,
    val_bigint bigint,
    val_money numeric(20,2),
    val_numeric numeric(20,3),
    val_ratio_num bigint,
    val_ratio_denum bigint,
    swop character varying(20) NOT NULL,
    swebdoc_id integer NOT NULL
);


--
-- TOC entry 200 (class 1259 OID 191453)
-- Name: swopva_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE swopva_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2354 (class 0 OID 0)
-- Dependencies: 200
-- Name: swopva_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE swopva_id_seq OWNED BY swopva.id;


--
-- TOC entry 201 (class 1259 OID 191455)
-- Name: tag; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE tag (
    id integer NOT NULL,
    tagname character varying(50),
    description text,
    parent_id integer,
    const boolean,
    constname character varying(50),
    fts tsvector
);


--
-- TOC entry 202 (class 1259 OID 191461)
-- Name: tag_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE tag_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2355 (class 0 OID 0)
-- Dependencies: 202
-- Name: tag_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE tag_id_seq OWNED BY tag.id;


--
-- TOC entry 203 (class 1259 OID 191463)
-- Name: tip; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE tip (
    id integer NOT NULL,
    tip character varying(20) NOT NULL,
    title character varying(20) NOT NULL,
    description text
);


--
-- TOC entry 204 (class 1259 OID 191469)
-- Name: tip_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE tip_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2356 (class 0 OID 0)
-- Dependencies: 204
-- Name: tip_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE tip_id_seq OWNED BY tip.id;


--
-- TOC entry 205 (class 1259 OID 191471)
-- Name: webdoc; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE webdoc (
    id integer NOT NULL,
    keyname character varying(512) NOT NULL,
    description text,
    tip character varying(20),
    ca_id integer,
    val_bigint bigint,
    val_money numeric(20,2),
    val_numeric numeric(20,3),
    val_ratio_num bigint,
    val_ratio_denum bigint,
    scod character varying(128),
    cdate timestamp with time zone,
    udate timestamp with time zone,
    web_title text,
    web_meta_subject text,
    web_meta_keywords text,
    web_meta_description text,
    web_top_description text,
    web_citems text,
    web_description text,
    web_title_image text,
    url1 text,
    url1flag boolean,
    ttitle text,
    fts tsvector,
    plan_text text,
    is_product boolean DEFAULT true NOT NULL,
    web_color_0 character varying(7),
    web_color_1 character varying(7),
    web_color_2 character varying(7),
    web_color_3 character varying(7),
    web_color_4 character varying(7),
    web_color_5 character varying(7)
);


--
-- TOC entry 206 (class 1259 OID 191478)
-- Name: webdoc_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE webdoc_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2357 (class 0 OID 0)
-- Dependencies: 206
-- Name: webdoc_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE webdoc_id_seq OWNED BY webdoc.id;


--
-- TOC entry 207 (class 1259 OID 191480)
-- Name: webdoctag; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE webdoctag (
    webdoc_id integer NOT NULL,
    tag_id integer NOT NULL
);


--
-- TOC entry 208 (class 1259 OID 191483)
-- Name: webrole; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE webrole (
    id integer NOT NULL,
    keyname character varying(50) NOT NULL,
    title character varying(255),
    description text
);


--
-- TOC entry 209 (class 1259 OID 191489)
-- Name: webrole_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE webrole_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2358 (class 0 OID 0)
-- Dependencies: 209
-- Name: webrole_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE webrole_id_seq OWNED BY webrole.id;


--
-- TOC entry 210 (class 1259 OID 191491)
-- Name: webuser; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE webuser (
    id integer NOT NULL,
    username character varying(50) NOT NULL,
    description text,
    password character varying(60)
);


--
-- TOC entry 211 (class 1259 OID 191497)
-- Name: webuser_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE webuser_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2359 (class 0 OID 0)
-- Dependencies: 211
-- Name: webuser_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE webuser_id_seq OWNED BY webuser.id;


--
-- TOC entry 212 (class 1259 OID 191499)
-- Name: webuserwebrole; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE webuserwebrole (
    webuser_id integer NOT NULL,
    webrole_id integer NOT NULL
);


--
-- TOC entry 213 (class 1259 OID 191502)
-- Name: zone; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE zone (
    id integer NOT NULL,
    keyname character varying(50) NOT NULL,
    title character varying(255),
    description text
);


--
-- TOC entry 214 (class 1259 OID 191508)
-- Name: zone_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE zone_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2360 (class 0 OID 0)
-- Dependencies: 214
-- Name: zone_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE zone_id_seq OWNED BY zone.id;


--
-- TOC entry 2037 (class 2604 OID 191510)
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY acc ALTER COLUMN id SET DEFAULT nextval('acc_id_seq'::regclass);


--
-- TOC entry 2038 (class 2604 OID 191511)
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY arl ALTER COLUMN id SET DEFAULT nextval('arl_id_seq'::regclass);


--
-- TOC entry 2039 (class 2604 OID 191512)
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY ca ALTER COLUMN id SET DEFAULT nextval('ca_id_seq'::regclass);


--
-- TOC entry 2041 (class 2604 OID 191513)
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY files ALTER COLUMN id SET DEFAULT nextval('files_id_seq'::regclass);


--
-- TOC entry 2042 (class 2604 OID 191514)
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY op ALTER COLUMN id SET DEFAULT nextval('op_id_seq'::regclass);


--
-- TOC entry 2044 (class 2604 OID 191515)
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY opi ALTER COLUMN id SET DEFAULT nextval('opi_id_seq'::regclass);


--
-- TOC entry 2045 (class 2604 OID 191516)
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY opt ALTER COLUMN id SET DEFAULT nextval('opt_id_seq'::regclass);


--
-- TOC entry 2046 (class 2604 OID 191517)
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY opttype ALTER COLUMN id SET DEFAULT nextval('opttype_id_seq'::regclass);


--
-- TOC entry 2047 (class 2604 OID 191518)
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY own ALTER COLUMN id SET DEFAULT nextval('own_id_seq'::regclass);


--
-- TOC entry 2049 (class 2604 OID 191519)
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY stext ALTER COLUMN id SET DEFAULT nextval('stext_id_seq'::regclass);


--
-- TOC entry 2050 (class 2604 OID 191520)
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY swop ALTER COLUMN id SET DEFAULT nextval('swop_id_seq'::regclass);


--
-- TOC entry 2051 (class 2604 OID 191521)
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY swopv ALTER COLUMN id SET DEFAULT nextval('swopv_id_seq'::regclass);


--
-- TOC entry 2052 (class 2604 OID 191522)
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY swopva ALTER COLUMN id SET DEFAULT nextval('swopva_id_seq'::regclass);


--
-- TOC entry 2053 (class 2604 OID 191523)
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY tag ALTER COLUMN id SET DEFAULT nextval('tag_id_seq'::regclass);


--
-- TOC entry 2054 (class 2604 OID 191524)
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY tip ALTER COLUMN id SET DEFAULT nextval('tip_id_seq'::regclass);


--
-- TOC entry 2056 (class 2604 OID 191525)
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY webdoc ALTER COLUMN id SET DEFAULT nextval('webdoc_id_seq'::regclass);


--
-- TOC entry 2057 (class 2604 OID 191526)
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY webrole ALTER COLUMN id SET DEFAULT nextval('webrole_id_seq'::regclass);


--
-- TOC entry 2058 (class 2604 OID 191527)
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY webuser ALTER COLUMN id SET DEFAULT nextval('webuser_id_seq'::regclass);


--
-- TOC entry 2059 (class 2604 OID 191528)
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY zone ALTER COLUMN id SET DEFAULT nextval('zone_id_seq'::regclass);


--
-- TOC entry 2061 (class 2606 OID 191613)
-- Name: acc_pk; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY acc
    ADD CONSTRAINT acc_pk PRIMARY KEY (id);


--
-- TOC entry 2063 (class 2606 OID 191615)
-- Name: acc_uk_for_next1; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY acc
    ADD CONSTRAINT acc_uk_for_next1 UNIQUE (id, zone_id, own_id, webdoc_id, ca_root_id, ca_id, ca_parent_id, tip);


--
-- TOC entry 2065 (class 2606 OID 191617)
-- Name: acc_zone_id_own_id_webdoc_id_ca_id; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY acc
    ADD CONSTRAINT acc_zone_id_own_id_webdoc_id_ca_id UNIQUE (zone_id, own_id, webdoc_id, ca_id);


--
-- TOC entry 2126 (class 2606 OID 191620)
-- Name: anytext_keyname_key; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY stext
    ADD CONSTRAINT anytext_keyname_key UNIQUE (keyname);


--
-- TOC entry 2128 (class 2606 OID 191622)
-- Name: anytext_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY stext
    ADD CONSTRAINT anytext_pkey PRIMARY KEY (id);


--
-- TOC entry 2069 (class 2606 OID 191624)
-- Name: arl_pk; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY arl
    ADD CONSTRAINT arl_pk PRIMARY KEY (id);


--
-- TOC entry 2071 (class 2606 OID 191628)
-- Name: arl_uk_1; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY arl
    ADD CONSTRAINT arl_uk_1 UNIQUE (keyname);


--
-- TOC entry 2073 (class 2606 OID 191630)
-- Name: arl_uk_2; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY arl
    ADD CONSTRAINT arl_uk_2 UNIQUE (title);


--
-- TOC entry 2075 (class 2606 OID 191632)
-- Name: ca_id_tip_uk; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY ca
    ADD CONSTRAINT ca_id_tip_uk UNIQUE (id, tip);


--
-- TOC entry 2077 (class 2606 OID 191634)
-- Name: ca_parent_id_keyname_uk; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY ca
    ADD CONSTRAINT ca_parent_id_keyname_uk UNIQUE (parent_id, keyname);


--
-- TOC entry 2079 (class 2606 OID 191636)
-- Name: ca_parent_id_title_uk; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY ca
    ADD CONSTRAINT ca_parent_id_title_uk UNIQUE (parent_id, title);


--
-- TOC entry 2081 (class 2606 OID 191638)
-- Name: ca_pk; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY ca
    ADD CONSTRAINT ca_pk PRIMARY KEY (id);


--
-- TOC entry 2083 (class 2606 OID 191640)
-- Name: ca_tip_id_root_id_parent_id_uk; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY ca
    ADD CONSTRAINT ca_tip_id_root_id_parent_id_uk UNIQUE (id, root_id, parent_id, tip);


--
-- TOC entry 2085 (class 2606 OID 191642)
-- Name: files_pk; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY files
    ADD CONSTRAINT files_pk PRIMARY KEY (id);


--
-- TOC entry 2089 (class 2606 OID 191644)
-- Name: files_rel_webdoc_uk; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY files_rel
    ADD CONSTRAINT files_rel_webdoc_uk UNIQUE (files_id, webdoc_id);


--
-- TOC entry 2087 (class 2606 OID 191646)
-- Name: files_uk1; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY files
    ADD CONSTRAINT files_uk1 UNIQUE (path);


--
-- TOC entry 2094 (class 2606 OID 191648)
-- Name: op_id_opt_opttype_uk; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY op
    ADD CONSTRAINT op_id_opt_opttype_uk UNIQUE (id, opt, opttype);


--
-- TOC entry 2096 (class 2606 OID 191650)
-- Name: op_pk; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY op
    ADD CONSTRAINT op_pk PRIMARY KEY (id);


--
-- TOC entry 2100 (class 2606 OID 191652)
-- Name: opi_id_acc_id_acc_version_id_tip_uk; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY opi
    ADD CONSTRAINT opi_id_acc_id_acc_version_id_tip_uk UNIQUE (id, acc_id, acc_version_id, tip);


--
-- TOC entry 2102 (class 2606 OID 191654)
-- Name: opi_pk; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY opi
    ADD CONSTRAINT opi_pk PRIMARY KEY (id);


--
-- TOC entry 2105 (class 2606 OID 191656)
-- Name: opiacc_pk; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY opiacc
    ADD CONSTRAINT opiacc_pk PRIMARY KEY (opi_id);


--
-- TOC entry 2188 (class 2606 OID 192878)
-- Name: opiattrs_pk; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY opiattrs
    ADD CONSTRAINT opiattrs_pk PRIMARY KEY (opi_id);


--
-- TOC entry 2108 (class 2606 OID 191658)
-- Name: opt_opt_opttype_uk; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY opt
    ADD CONSTRAINT opt_opt_opttype_uk UNIQUE (opt, opttype);


--
-- TOC entry 2110 (class 2606 OID 191660)
-- Name: opt_pk; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY opt
    ADD CONSTRAINT opt_pk PRIMARY KEY (id);


--
-- TOC entry 2112 (class 2606 OID 191662)
-- Name: opt_tip_uk; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY opt
    ADD CONSTRAINT opt_tip_uk UNIQUE (opt);


--
-- TOC entry 2114 (class 2606 OID 191664)
-- Name: opt_title_uk; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY opt
    ADD CONSTRAINT opt_title_uk UNIQUE (title);


--
-- TOC entry 2116 (class 2606 OID 191666)
-- Name: opttype_pk; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY opttype
    ADD CONSTRAINT opttype_pk PRIMARY KEY (id);


--
-- TOC entry 2118 (class 2606 OID 191668)
-- Name: opttype_tip_uk; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY opttype
    ADD CONSTRAINT opttype_tip_uk UNIQUE (opttype);


--
-- TOC entry 2120 (class 2606 OID 191670)
-- Name: opttype_title_uk; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY opttype
    ADD CONSTRAINT opttype_title_uk UNIQUE (title);


--
-- TOC entry 2122 (class 2606 OID 191672)
-- Name: own_pk; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY own
    ADD CONSTRAINT own_pk PRIMARY KEY (id);


--
-- TOC entry 2124 (class 2606 OID 191674)
-- Name: ownarl_uk; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY ownarl
    ADD CONSTRAINT ownarl_uk PRIMARY KEY (own_id, arl_id);


--
-- TOC entry 2134 (class 2606 OID 191676)
-- Name: swapv_uk1; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY swopv
    ADD CONSTRAINT swapv_uk1 UNIQUE (id, swop, swebdoc_id);


--
-- TOC entry 2130 (class 2606 OID 191678)
-- Name: swop_pk; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY swop
    ADD CONSTRAINT swop_pk PRIMARY KEY (id);


--
-- TOC entry 2132 (class 2606 OID 191680)
-- Name: swop_uk; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY swop
    ADD CONSTRAINT swop_uk UNIQUE (swop);


--
-- TOC entry 2136 (class 2606 OID 191682)
-- Name: swopv_pk; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY swopv
    ADD CONSTRAINT swopv_pk PRIMARY KEY (id);


--
-- TOC entry 2138 (class 2606 OID 191684)
-- Name: swopv_swop_opttype_opt_swebdoc_id_uk1; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY swopv
    ADD CONSTRAINT swopv_swop_opttype_opt_swebdoc_id_uk1 UNIQUE (swop, opttype, opt, swebdoc_id);


--
-- TOC entry 2142 (class 2606 OID 191686)
-- Name: swopva_pk; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY swopva
    ADD CONSTRAINT swopva_pk PRIMARY KEY (id);


--
-- TOC entry 2144 (class 2606 OID 191688)
-- Name: swopva_swopv_id_keyname_uk1; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY swopva
    ADD CONSTRAINT swopva_swopv_id_keyname_uk1 UNIQUE (swopv_id, keyname);


--
-- TOC entry 2147 (class 2606 OID 191690)
-- Name: tag_pk; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY tag
    ADD CONSTRAINT tag_pk PRIMARY KEY (id);


--
-- TOC entry 2149 (class 2606 OID 191692)
-- Name: tag_uk1; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY tag
    ADD CONSTRAINT tag_uk1 UNIQUE (tagname);


--
-- TOC entry 2151 (class 2606 OID 191694)
-- Name: tip_pk; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY tip
    ADD CONSTRAINT tip_pk PRIMARY KEY (id);


--
-- TOC entry 2153 (class 2606 OID 191696)
-- Name: tip_tip_uk; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY tip
    ADD CONSTRAINT tip_tip_uk UNIQUE (tip);


--
-- TOC entry 2155 (class 2606 OID 191698)
-- Name: tip_title_uk; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY tip
    ADD CONSTRAINT tip_title_uk UNIQUE (title);


--
-- TOC entry 2157 (class 2606 OID 191700)
-- Name: webdoc_id_tip_ca_id; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY webdoc
    ADD CONSTRAINT webdoc_id_tip_ca_id UNIQUE (id, tip, ca_id);


--
-- TOC entry 2159 (class 2606 OID 191702)
-- Name: webdoc_id_val_ratio_denum_uk; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY webdoc
    ADD CONSTRAINT webdoc_id_val_ratio_denum_uk UNIQUE (id, val_ratio_denum);


--
-- TOC entry 2161 (class 2606 OID 191704)
-- Name: webdoc_pk; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY webdoc
    ADD CONSTRAINT webdoc_pk PRIMARY KEY (id);


--
-- TOC entry 2163 (class 2606 OID 191706)
-- Name: webdoc_scod; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY webdoc
    ADD CONSTRAINT webdoc_scod UNIQUE (scod);


--
-- TOC entry 2165 (class 2606 OID 191708)
-- Name: webdoctag_pk; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY webdoctag
    ADD CONSTRAINT webdoctag_pk PRIMARY KEY (webdoc_id, tag_id);


--
-- TOC entry 2167 (class 2606 OID 191710)
-- Name: webrole_pk; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY webrole
    ADD CONSTRAINT webrole_pk PRIMARY KEY (id);


--
-- TOC entry 2169 (class 2606 OID 191712)
-- Name: webrole_uk_1; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY webrole
    ADD CONSTRAINT webrole_uk_1 UNIQUE (keyname);


--
-- TOC entry 2171 (class 2606 OID 191714)
-- Name: webrole_uk_2; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY webrole
    ADD CONSTRAINT webrole_uk_2 UNIQUE (title);


--
-- TOC entry 2173 (class 2606 OID 191716)
-- Name: webuser_pk; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY webuser
    ADD CONSTRAINT webuser_pk PRIMARY KEY (id);


--
-- TOC entry 2175 (class 2606 OID 191718)
-- Name: webuser_username_uk; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY webuser
    ADD CONSTRAINT webuser_username_uk UNIQUE (username);


--
-- TOC entry 2177 (class 2606 OID 191720)
-- Name: webuserwebrole_uk; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY webuserwebrole
    ADD CONSTRAINT webuserwebrole_uk PRIMARY KEY (webuser_id, webrole_id);


--
-- TOC entry 2179 (class 2606 OID 191722)
-- Name: zone_pk; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY zone
    ADD CONSTRAINT zone_pk PRIMARY KEY (id);


--
-- TOC entry 2181 (class 2606 OID 191724)
-- Name: zone_uk3; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY zone
    ADD CONSTRAINT zone_uk3 UNIQUE (title);


--
-- TOC entry 2183 (class 2606 OID 191726)
-- Name: zone_uk_1; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY zone
    ADD CONSTRAINT zone_uk_1 UNIQUE (id, keyname);


--
-- TOC entry 2185 (class 2606 OID 191728)
-- Name: zone_uk_2; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY zone
    ADD CONSTRAINT zone_uk_2 UNIQUE (keyname);


--
-- TOC entry 2066 (class 1259 OID 191729)
-- Name: fki_acc_webdoc_id_val_ratio_denum_fk1; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX fki_acc_webdoc_id_val_ratio_denum_fk1 ON acc USING btree (webdoc_id, val_ratio_denum);


--
-- TOC entry 2067 (class 1259 OID 191730)
-- Name: fki_acc_zone_id_fk; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX fki_acc_zone_id_fk ON acc USING btree (zone_id);


--
-- TOC entry 2103 (class 1259 OID 191731)
-- Name: fki_apiacc_opi_id_acc_id_acc_version_id_tip_uk_fk; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX fki_apiacc_opi_id_acc_id_acc_version_id_tip_uk_fk ON opiacc USING btree (opi_id, id, version_id, tip);


--
-- TOC entry 2186 (class 1259 OID 192884)
-- Name: fki_apiacc_opi_id_keyname_uk_fk; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX fki_apiacc_opi_id_keyname_uk_fk ON opiattrs USING btree (opi_id, keyname);


--
-- TOC entry 2090 (class 1259 OID 191732)
-- Name: fki_files_rel_webdoc_fk; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX fki_files_rel_webdoc_fk ON files_rel USING btree (webdoc_id);


--
-- TOC entry 2091 (class 1259 OID 191733)
-- Name: fki_op_opt_fk; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX fki_op_opt_fk ON op USING btree (opt);


--
-- TOC entry 2092 (class 1259 OID 191734)
-- Name: fki_op_opt_opttype_fk; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX fki_op_opt_opttype_fk ON op USING btree (opt, opttype);


--
-- TOC entry 2097 (class 1259 OID 191735)
-- Name: fki_opi_acc_uk_for_next1; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX fki_opi_acc_uk_for_next1 ON opi USING btree (acc_id, zone_id, own_id, webdoc_id, ca_root_id, ca_id, ca_parent_id, tip);


--
-- TOC entry 2098 (class 1259 OID 191736)
-- Name: fki_opi_op_id_opt_opttype_uk; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX fki_opi_op_id_opt_opttype_uk ON opi USING btree (op_id, opt, opttype);


--
-- TOC entry 2106 (class 1259 OID 191737)
-- Name: fki_opttip_id_fk; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX fki_opttip_id_fk ON opt USING btree (opttype);


--
-- TOC entry 2139 (class 1259 OID 191738)
-- Name: fki_swopva_swap_swopv_id_fk1; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX fki_swopva_swap_swopv_id_fk1 ON swopva USING btree (swopv_id, swop);


--
-- TOC entry 2140 (class 1259 OID 191739)
-- Name: fki_swopva_swap_swopv_id_swebdoc_id_fk1; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX fki_swopva_swap_swopv_id_swebdoc_id_fk1 ON swopva USING btree (swopv_id, swop, swebdoc_id);


--
-- TOC entry 2145 (class 1259 OID 191740)
-- Name: fki_tag_tree_fk; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX fki_tag_tree_fk ON tag USING btree (parent_id);


--
-- TOC entry 2223 (class 2620 OID 191741)
-- Name: own_fts_indexation_tg; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER own_fts_indexation_tg BEFORE INSERT OR UPDATE ON own FOR EACH ROW EXECUTE PROCEDURE own_fts_indexation();


--
-- TOC entry 2224 (class 2620 OID 191742)
-- Name: stext_fts_indexation_tg; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER stext_fts_indexation_tg BEFORE INSERT OR UPDATE ON stext FOR EACH ROW EXECUTE PROCEDURE stext_fts_indexation();


--
-- TOC entry 2225 (class 2620 OID 191743)
-- Name: webdoc_fts_indexation_tg; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER webdoc_fts_indexation_tg BEFORE INSERT OR UPDATE ON webdoc FOR EACH ROW EXECUTE PROCEDURE webdoc_fts_indexation();


--
-- TOC entry 2189 (class 2606 OID 191744)
-- Name: acc_ca_id_ca_root_id_ca_parent_id_tip_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY acc
    ADD CONSTRAINT acc_ca_id_ca_root_id_ca_parent_id_tip_fk FOREIGN KEY (ca_id, ca_root_id, tip, ca_parent_id) REFERENCES ca(id, root_id, tip, parent_id);


--
-- TOC entry 2190 (class 2606 OID 191749)
-- Name: acc_own_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY acc
    ADD CONSTRAINT acc_own_fk FOREIGN KEY (own_id) REFERENCES own(id);


--
-- TOC entry 2191 (class 2606 OID 191754)
-- Name: acc_webdoc_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY acc
    ADD CONSTRAINT acc_webdoc_id_fk FOREIGN KEY (webdoc_id) REFERENCES webdoc(id);


--
-- TOC entry 2192 (class 2606 OID 191759)
-- Name: acc_webdoc_id_tip_ca_root_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY acc
    ADD CONSTRAINT acc_webdoc_id_tip_ca_root_id FOREIGN KEY (webdoc_id, tip, ca_root_id) REFERENCES webdoc(id, tip, ca_id);


--
-- TOC entry 2193 (class 2606 OID 191764)
-- Name: acc_webdoc_id_val_ratio_denum_fk1; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY acc
    ADD CONSTRAINT acc_webdoc_id_val_ratio_denum_fk1 FOREIGN KEY (webdoc_id, val_ratio_denum) REFERENCES webdoc(id, val_ratio_denum);


--
-- TOC entry 2194 (class 2606 OID 191769)
-- Name: acc_zone_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY acc
    ADD CONSTRAINT acc_zone_id_fk FOREIGN KEY (zone_id) REFERENCES zone(id);


--
-- TOC entry 2205 (class 2606 OID 191774)
-- Name: apiacc_opi_id_acc_id_acc_version_id_tip_uk_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY opiacc
    ADD CONSTRAINT apiacc_opi_id_acc_id_acc_version_id_tip_uk_fk FOREIGN KEY (opi_id, id, version_id, tip) REFERENCES opi(id, acc_id, acc_version_id, tip);


--
-- TOC entry 2222 (class 2606 OID 192879)
-- Name: apiacc_opi_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY opiattrs
    ADD CONSTRAINT apiacc_opi_id_fk FOREIGN KEY (opi_id) REFERENCES opi(id);


--
-- TOC entry 2195 (class 2606 OID 191779)
-- Name: ca_parent_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY ca
    ADD CONSTRAINT ca_parent_id_fk FOREIGN KEY (parent_id) REFERENCES ca(id);


--
-- TOC entry 2196 (class 2606 OID 191784)
-- Name: ca_root_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY ca
    ADD CONSTRAINT ca_root_id_fk FOREIGN KEY (root_id) REFERENCES ca(id);


--
-- TOC entry 2197 (class 2606 OID 191789)
-- Name: ca_tip_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY ca
    ADD CONSTRAINT ca_tip_fk FOREIGN KEY (tip) REFERENCES tip(tip);


--
-- TOC entry 2198 (class 2606 OID 191794)
-- Name: files_rel_to_files_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY files_rel
    ADD CONSTRAINT files_rel_to_files_id_fk FOREIGN KEY (files_id) REFERENCES files(id);


--
-- TOC entry 2199 (class 2606 OID 191799)
-- Name: files_rel_webdoc_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY files_rel
    ADD CONSTRAINT files_rel_webdoc_fk FOREIGN KEY (webdoc_id) REFERENCES webdoc(id);


--
-- TOC entry 2200 (class 2606 OID 191804)
-- Name: op_opt_opttype_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY op
    ADD CONSTRAINT op_opt_opttype_fk FOREIGN KEY (opt, opttype) REFERENCES opt(opt, opttype);


--
-- TOC entry 2201 (class 2606 OID 191809)
-- Name: op_prev_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY op
    ADD CONSTRAINT op_prev_id_fk FOREIGN KEY (prev_id) REFERENCES op(id);


--
-- TOC entry 2202 (class 2606 OID 191814)
-- Name: op_webuserer_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY op
    ADD CONSTRAINT op_webuserer_id_fk FOREIGN KEY (webuser_id) REFERENCES webuser(id);


--
-- TOC entry 2203 (class 2606 OID 191819)
-- Name: opi_acc_uk_for_next1; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY opi
    ADD CONSTRAINT opi_acc_uk_for_next1 FOREIGN KEY (acc_id, zone_id, own_id, webdoc_id, ca_root_id, ca_id, ca_parent_id, tip) REFERENCES acc(id, zone_id, own_id, webdoc_id, ca_root_id, ca_id, ca_parent_id, tip);


--
-- TOC entry 2204 (class 2606 OID 191824)
-- Name: opi_op_id_opt_opttype_uk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY opi
    ADD CONSTRAINT opi_op_id_opt_opttype_uk FOREIGN KEY (op_id, opt, opttype) REFERENCES op(id, opt, opttype);


--
-- TOC entry 2206 (class 2606 OID 191829)
-- Name: opttype_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY opt
    ADD CONSTRAINT opttype_id_fk FOREIGN KEY (opttype) REFERENCES opttype(opttype);


--
-- TOC entry 2207 (class 2606 OID 191834)
-- Name: ownarl_arl_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY ownarl
    ADD CONSTRAINT ownarl_arl_id_fk FOREIGN KEY (arl_id) REFERENCES arl(id);


--
-- TOC entry 2208 (class 2606 OID 191839)
-- Name: ownarl_own_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY ownarl
    ADD CONSTRAINT ownarl_own_id_fk FOREIGN KEY (own_id) REFERENCES own(id);


--
-- TOC entry 2209 (class 2606 OID 191844)
-- Name: swopv_opt_opttype_uk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY swopv
    ADD CONSTRAINT swopv_opt_opttype_uk FOREIGN KEY (opt, opttype) REFERENCES opt(opt, opttype);


--
-- TOC entry 2210 (class 2606 OID 191849)
-- Name: swopv_swebdoc_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY swopv
    ADD CONSTRAINT swopv_swebdoc_id FOREIGN KEY (swebdoc_id) REFERENCES webdoc(id);


--
-- TOC entry 2211 (class 2606 OID 191854)
-- Name: swopv_swop_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY swopv
    ADD CONSTRAINT swopv_swop_fk FOREIGN KEY (swop) REFERENCES swop(swop);


--
-- TOC entry 2212 (class 2606 OID 191859)
-- Name: swopva_swap_swopv_id_swebdoc_id_fk1; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY swopva
    ADD CONSTRAINT swopva_swap_swopv_id_swebdoc_id_fk1 FOREIGN KEY (swopv_id, swop, swebdoc_id) REFERENCES swopv(id, swop, swebdoc_id);


--
-- TOC entry 2213 (class 2606 OID 191864)
-- Name: swopva_swopv_id_fk1; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY swopva
    ADD CONSTRAINT swopva_swopv_id_fk1 FOREIGN KEY (swopv_id) REFERENCES swopv(id);


--
-- TOC entry 2214 (class 2606 OID 191869)
-- Name: swopva_webdoc_id_tip_ca_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY swopva
    ADD CONSTRAINT swopva_webdoc_id_tip_ca_id FOREIGN KEY (webdoc_id, tip, ca_id) REFERENCES webdoc(id, tip, ca_id);


--
-- TOC entry 2215 (class 2606 OID 191874)
-- Name: swopva_webdoc_id_val_ratio_denum_fk1; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY swopva
    ADD CONSTRAINT swopva_webdoc_id_val_ratio_denum_fk1 FOREIGN KEY (webdoc_id, val_ratio_denum) REFERENCES webdoc(id, val_ratio_denum);


--
-- TOC entry 2216 (class 2606 OID 191879)
-- Name: tag_tree_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY tag
    ADD CONSTRAINT tag_tree_fk FOREIGN KEY (parent_id) REFERENCES tag(id);


--
-- TOC entry 2217 (class 2606 OID 191884)
-- Name: webdoc_tip_ca_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY webdoc
    ADD CONSTRAINT webdoc_tip_ca_fk FOREIGN KEY (tip, ca_id) REFERENCES ca(tip, id);


--
-- TOC entry 2218 (class 2606 OID 191889)
-- Name: webdoctag_tag_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY webdoctag
    ADD CONSTRAINT webdoctag_tag_id FOREIGN KEY (tag_id) REFERENCES tag(id);


--
-- TOC entry 2219 (class 2606 OID 191894)
-- Name: webdoctag_webdoc_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY webdoctag
    ADD CONSTRAINT webdoctag_webdoc_id FOREIGN KEY (webdoc_id) REFERENCES webdoc(id);


--
-- TOC entry 2220 (class 2606 OID 191899)
-- Name: webuserwebrole_webrole_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY webuserwebrole
    ADD CONSTRAINT webuserwebrole_webrole_id_fk FOREIGN KEY (webrole_id) REFERENCES webrole(id);


--
-- TOC entry 2221 (class 2606 OID 191904)
-- Name: webuserwebrole_webuser_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY webuserwebrole
    ADD CONSTRAINT webuserwebrole_webuser_id_fk FOREIGN KEY (webuser_id) REFERENCES webuser(id);


-- Completed on 2015-08-28 17:51:19 ALMT

--
-- PostgreSQL database dump complete
--

