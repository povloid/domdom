#/sbin/sh

export PATH=$PATH:/opt/PostgreSQL/9.3/bin/
export PGPASSWORD="paradox"

dropdb -e -h localhost -U domdom -p 5433 domdom
createdb -h localhost -U domdom -p 5433 -e -E UTF8 -T template0 domdom
psql -h localhost -U domdom -p 5433 -f schema.sql domdom

