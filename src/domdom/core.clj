(ns domdom.core
  (:use korma.db)
  (:use korma.core)
  (:use clojure.pprint)

  (:require
   [ixinfestor.core :as ix]
   [tarsonis.core :as tc]

   )
  )

(println "Инициализация схемы.....")

;;**************************************************************************************************
;;* BEGIN Data base initialisation
;;* tag: <database initialisation connection>
;;*
;;* description: инициализация базы данных
;;*
;;**************************************************************************************************
(korma.db/defdb domdom (korma.db/postgres {:db "domdom"
                                           :user "domdom"
                                           :password "paradox"
                                           ;; optional keys
                                           :host "localhost"
                                           :port "5433"}))

(deliver ixinfestor.core/files-root-directory "files_root_directory/domdom-data")

;; END Data base initialisation
;;..................................................................................................

(def webrolesgroup-common
  (ix/webrolesgroup-init {:keyname :common
                          :title "Основная группа ролей"}))



(def webrole-admin
  (ix/webrole-init {:keyname :admin
                    :title "Адмнистратор"
                    :description "Управление рользователями и настроуками сайта"
                    :webrolesgroup_id (webrolesgroup-common :id)}))
(def webrole-user
  (ix/webrole-init {:keyname :user
                    :title "Пользователь"
                    :description "Разрешен вход в систему"
                    :webrolesgroup_id (webrolesgroup-common :id)}))
(def webrole-content-manager
  (ix/webrole-init {:keyname :content-manager
                    :title "Редактор контента"
                    :description "Разрешено редактирование контента"
                    :webrolesgroup_id (webrolesgroup-common :id)}))


;;**************************************************************************************************
;;* BEGIN tips and counted attributes
;;* tag: <tips and couted attributes>
;;*
;;* description: Описание дополнительных типов и счетных атрибутов
;;*
;;**************************************************************************************************


;; инициализируем стандартные типы
(tc/install-tips)


;; предопределяем счетные атрибуты
(tc/create-ca-as-list
 (merge
  tc/ca-standart-coll
  {
   :shoe-size {
               :title "Размеры обуви"
               :tip :bigint
               :description "Размеры обуви..."
               :sub {
                     :29 {:title "29 размер"}
                     :30 {:title "30 размер"}
                     :31 {:title "31 размер"}
                     :32 {:title "32 размер"}
                     :33 {:title "33 размер"}
                     }
               }
   :colored-size {
                  :title "цвета предметов"
                  :tip :ratio
                  :description "цвета предметов..."
                  :sub {
                        :red {
                              :title "красный"
                              :sub {
                                    :17 {:title "17 размер"}
                                    :18 {:title "18 размер"}
                                    :19 {:title "19 размер"}
                                    }
                              }

                        :green {
                                :title "зеленый"
                                :sub {
                                      :17 {:title "17 размер"}
                                      :18 {:title "18 размер"}
                                      :19 {:title "19 размер"}
                                      }
                                }

                        :blue {
                               :title "голубой"
                               :sub {
                                     :17 {:title "17 размер"}
                                     :18 {:title "18 размер"}
                                     :19 {:title "19 размер"}
                                     }
                               }
                        }
                  }
   }))

;; END tips and counted attributes
;;..................................................................................................


;;**************************************************************************************************
;;* BEGIN Roles
;;* tag: <roles>
;;*
;;* description: определяем роли для схемы учета
;;*
;;**************************************************************************************************

(def arole-client   (tc/arole :client   "Клиент"    "Клиенты, которые совершают покупки"))
(def arole-supplier (tc/arole :supplier "Поставщик" "Поставщик продукции"))
(def arole-company  (tc/arole :company  "Компания" "Представляет собственность компании"))

;; END Roles
;;..................................................................................................

;;**************************************************************************************************
;;* BEGIN Predefined owners
;;* tag: <predefined owners>
;;*
;;* description: Предопределяем некоторых владельцев счетов
;;*
;;**************************************************************************************************

(def own-company (tc/static-own {:keyname "компания"}
                                [:company]))

(def own-supplier-noname (tc/static-own {:keyname "Неизвесный поставщик"}
                                        [:supplier]))

(def own-client-noname (tc/static-own {:keyname "неизвесный клиент"}
                                      [:client]))

;; END predefined owners
;;..................................................................................................


;;**************************************************************************************************
;;* BEGIN AZones
;;* tag: <azones>
;;*
;;* description: Описываем зоны учета
;;*
;;**************************************************************************************************

;; Объявляем новую схему учета !!!
(tc/main-scheme-new)

;; Основная зона учета
(def azone-main
  (tc/azone {:keyname :main
             :sql-prefix :main
             :title "Основная зона учета"
             :path-set #{}
             :description "Данная зона учета охватывает все остальные зоны учета."}))

;;------------------------------------------------------------------------------
;; BEGIN: External azone
;; tag: <external azone>
;; description: Зоны учета для внешних счетов
;;------------------------------------------------------------------------------

(def azone-external
  (tc/azone {:keyname :external
             :sql-prefix :external
             :title "Внешняя зона"
             :path-set (tc/has-in azone-main)
             :description "Зона учета, в которую входит все внешние счета, которые не относятся в компании"}))

(def azone-suppliers
  (tc/azone {:keyname :suppliers
             :sql-prefix :suppliers
             :title "поставщики"
             :path-set (tc/has-in azone-external)
             :aroles-set #{arole-supplier}
             :validators []
             :description "поставщики товаров"}))

(def azone-clients
  (tc/azone {:keyname :clients
             :sql-prefix :clients
             :title "клиенты"
             :path-set  (tc/has-in azone-external)
             :aroles-set #{arole-client}
             :rules []
             :description "наши дорогие клиенты"}))


(def azone-black-hole
  (tc/azone {:keyname :black-hole
             :sql-prefix :black-hole
             :title "Черная дыра"
             :path-set  (tc/has-in azone-external)
             :aroles-set #{arole-company}
             :rules []
             :description "Внешние нужды и источники денег"}))

;; END External azone
;;..............................................................................

;;------------------------------------------------------------------------------
;; BEGIN: Internal azones
;; tag: <internal azones>
;; description: Зоны для внутренних счетов компании
;;------------------------------------------------------------------------------

(def azone-internal
  (tc/azone {:keyname :internal
             :sql-prefix :internal
             :title "Внутренняя зона учета"
             :path-set (tc/has-in azone-main)
             :description "Зона, содержащая счета принадлежащие компании"}))

;;----------------------------------------------------------
;; BEGIN: Clients balance
;; tag: <client balance>
;; description: Баланс клиентов
;;----------------------------------------------------------

;; Баланс клиентов
(def azone-client-balance
  (tc/azone {:keyname :client-balance
             :sql-prefix :client_balance
             :title "Баланс клинта"
             :path-set (tc/has-in azone-internal)
             :aroles-set #{arole-company}
             :rules []
             :description "Баланс клиентов"}))

;; END Clients balance
;;..........................................................

;;----------------------------------------------------------
;; BEGIN: Warehouse-1
;; tag: <warehouse-1>
;; description: Склад 1
;;----------------------------------------------------------

(def azone-warehouse-1
  (tc/azone {:keyname :warehouse-1
             :sql-prefix :warehouse_1
             :title "Склад №1"
             :path-set (tc/has-in azone-internal)
             :aroles-set #{arole-company}
             :rules [tc/rule-zone-vals>0  ;; учет физических товаров
                     (tc/rule-zone-tips #{:bigint :ratio :numeric})]
             :description "Склад предприятия №1"}))

;; END Warehouse-1
;;..........................................................

;;----------------------------------------------------------
;; BEGIN: Store 1
;; tag: <store-1>
;; description: Магазин №1
;;----------------------------------------------------------

(def azone-store-1
  (tc/azone {:keyname :store-1
             :sql-prefix :store_1
             :title "Магазин №1"
             :path-set (tc/has-in azone-internal)
             :aroles-set #{}
             :description "Магазин предприятия"}))

;; --------------------------------------------------------
;; Реальные активы, которые есть в магазине ---------------
(def azone-store-1-phisical
  (tc/azone {:keyname :store-1-phisical
             :sql-prefix :store_i_phisical
             :title "Физические счета в магазине"
             :path-set (tc/has-in azone-store-1)
             :rules [tc/rule-zone-vals>0]
             :description "Все что имеются в магазине"}))

;; Товары --------------------------------------------------
(def azone-store-1-phisical-products
  (tc/azone {:keyname :store-1-phisical-products
             :sql-prefix :store_1_phisical_products
             :title  "Товары"
             :path-set (tc/has-in azone-store-1-phisical)
             :rules [(tc/rule-zone-tips #{:bigint :numeric :ratio})]
             :description "Товары в магазине"}))

(def azone-store-1-phisical-products-for-sale
  (tc/azone {:keyname :store-1-phisical-products-for-sale
             :sql-prefix :store_1_phisical_products_for_sale
             :title "Товары на продажу"
             :path-set (tc/has-in azone-store-1-phisical-products)
             :aroles-set #{arole-company}
             :rules []
             :description "Товары которые можно продать"}))

(def azone-store-1-phisical-products-saled
  (tc/azone {:keyname :store-1-phisical-products-saled
             :sql-prefix :store_1_phisical_products_saled
             :title "Товары проданные"
             :path-set (tc/has-in azone-store-1-phisical-products)
             :aroles-set #{arole-client}
             :description "Товары которые ждут клиента"}))

;; Деньги --------------------------------------------------
(def azone-store-1-phisical-money
  (tc/azone {:keyname :store-1-phisical-money
             :sql-prefix :store_1_phisical_money
             :title  "Деньги в магазине №1"
             :path-set (tc/has-in azone-store-1-phisical)
             :rules [(tc/rule-zone-tips #{:money})]
             :description "Все деньги в магазине №1"}))

(def azone-store-1-phisical-money-cash
  (tc/azone {:keyname :store-1-phisical-money-cash
             :sql-prefix :store_1_phisical_money_cash
             :title   "Касса №1"
             :path-set (tc/has-in azone-store-1-phisical-money)
             :aroles-set #{arole-company}
             :description "Деньги в кассе №1"}))

;; END Store 1
;;..........................................................
;; END Internal azones
;;..............................................................................
;; END AZones
;;..................................................................................................

;;**************************************************************************************************
;;* BEGIN Directions
;;* tag: <directions>
;;*
;;* description: Описание направлений для проведения операций
;;*
;;**************************************************************************************************
                                        ;TODO: Перенести это дело в тесты!!!


(def direction->-azone-store-1-phisical-products-for-sale->>-azone-clients
  (tc/direction-> azone-store-1-phisical-products-for-sale azone-clients))

(def direction->-azone-clients->>-azone-store-1-phisical-money-cash
  (tc/direction-> azone-clients azone-store-1-phisical-money-cash))

(def direction->-azone-suppliers->>-azone-warehouse-1
  (tc/direction-> azone-suppliers azone-warehouse-1))

(def direction->-azone-warehouse-1->>-azone-store-1-phisical-products-for-sale
  (tc/direction-> azone-warehouse-1 azone-store-1-phisical-products-for-sale))


(def azone-black-hole->>-azone-store-1-phisical-money-cash
  (tc/direction-> azone-black-hole azone-store-1-phisical-money-cash))
(def azone-store-1-phisical-money-cash->>-azone-black-hole
  (tc/direction-> azone-store-1-phisical-money-cash azone-black-hole))


;; END Directions
;;..................................................................................................


;;**************************************************************************************************
;;* BEGIN trl - user roles
;;* tag: <trl>
;;*
;;* description: определение ролей доступа
;;*
;;**************************************************************************************************



(def webrolesgroup-trade
  (ix/webrolesgroup-init {:keyname :trade
                          :title "Торговая группа ролей"}))

(def trl-warehouser-1
  (ix/webrole-init {:keyname :warehouser-1
                    :title "кладовщик скалада №1"
                    :description "задает права для проведения операци на складе №1"
                    :webrolesgroup_id (webrolesgroup-trade :id)}))
(def trl-warehouser-1-key (:keyname trl-warehouser-1))



(def trl-seller-1
  (ix/webrole-init {:keyname :seller-1
                    :title "продавец магазина №1"
                    :description "задает права для проведения операци в магазине №1"
                    :webrolesgroup_id (webrolesgroup-trade :id)}))
(def trl-seller-1-key (:keyname trl-seller-1))


;; END trl
;;..................................................................................................


;;**************************************************************************************************
;;* BEGIN predefined counted objects
;;* tag: <predefined counted objects>
;;*
;;* description: Предопределенные счетные атрибуты
;;*
;;**************************************************************************************************

(println (tc/ax-cr :money 1))

(def currency-KZT (tc/webdoc-save-by-scod {:keyname "Казахстанский тенге"
                                           :scod "KZT"
                                           :tip :money
                                           :val (tc/ax-cr :money 1)
                                           :ca_id (:id (tc/ca-get :standart-money))
                                           :description  "Денежная единица Республики Казахстан"}))

;; END predefined counted objects
;;..................................................................................................


;;**************************************************************************************************
;;* BEGIN SWOP description
;;* tag: <swop description>
;;*
;;* description: Описание плитики обмен
;;*
;;**************************************************************************************************

(def swop-1 (tc/swop-describe
             {tc/key-swop :swop-1
              tc/key-swop-sql-prefix :swop_1
              tc/key-title "Без скидки"
              tc/key-description "Данная операция производися напрямую без скидки"
              tc/key-swop-variants {:currency currency-KZT}
              :calc (fn [ax-quantity ax-base]
                      )
              :validator (fn []
                           )
              }))

(def swop-spec-1 (tc/swop-describe
                  {tc/key-swop :swop-spec-1
                   tc/key-swop-sql-prefix :swop_spec_1
                   tc/key-title "Спец цена 1"
                   tc/key-description "Специальная цена для диллеров"
                   tc/key-swop-variants {:currency currency-KZT}
                   :calc (fn [ax-quantity ax-base]
                           )
                   :validator (fn []
                                )
                   }))

;; END SWOP description
;;..................................................................................................

;;**************************************************************************************************
;;* BEGIN Operations
;;* tag: <operations>
;;*
;;* description: Определение операций
;;*
;;**************************************************************************************************


;; инициализируем стандартные типы операций
(def opttype-prod+  (tc/opttype-std-prod+ ))
(def opttype-prod-  (tc/opttype-std-prod- ))
(def opttype-prod-> (tc/opttype-std-prod->))
(def opttype-money+ (tc/opttype-std-money+))
(def opttype-money- (tc/opttype-std-money-))
(def opttype-prod$  (tc/opttype-std-prod$ ))



(def opt-prod+-to-warehouse-1-from-suppliers
  (tc/opt-describe {:opt :opt-prod+-to-warehouse-1-from-suppliers
                    :title "Приход товара на склад 1"
                    :opttype opttype-prod+
                    :description "поступление товара на склад..."

                    ;; задаем роли, кому разрешена операция
                    :troles-set #{trl-warehouser-1-key}


                    :owns {:from {:default own-supplier-noname
                                  :alt-search-in #{:supplier}}
                           :to {:default own-company}}

                    ;; Возможные направления
                    :directions-set #{direction->-azone-suppliers->>-azone-warehouse-1}


                    ;; Список правил для операции
                    :rules-for-operatio [
                                         ;; Проверка типов перемещений по направлениям
                                         (tc/rule-o-boxes-directions-tips
                                          direction->-azone-suppliers->>-azone-warehouse-1
                                          #{:ratio :numeric :bigint})
                                         ]


                    :products {:search-for-tips #{:ratio :numeric :bigint}
                               :selected-list []
                               :direction direction->-azone-suppliers->>-azone-warehouse-1}



                    }))



(def opt-warehouse-1--prod->--store-1
  (tc/opt-describe {:opt :opt-warehouse-1--prod->--store-1
                    :title "перемещение товара со склада 1 в магазин 1"
                    :opttype opttype-prod->
                    :description "простая операция перемещения....."

                    ;; задаем роли, кому разрешена операция
                    :troles-set #{trl-warehouser-1-key}


                    :owns {:from {:default own-company}
                           :to {:default own-company}}

                    :directions-set #{direction->-azone-warehouse-1->>-azone-store-1-phisical-products-for-sale}


                    :rules-for-operatio [(tc/rule-o-boxes-directions-tips
                                          direction->-azone-warehouse-1->>-azone-store-1-phisical-products-for-sale
                                          #{:ratio :numeric :bigint})
                                         ]

                    :products {:search-for-tips #{:ratio :numeric :bigint}
                               :selected-list []
                               :direction direction->-azone-warehouse-1->>-azone-store-1-phisical-products-for-sale}


                    }))




(def opt-store-1--money+
  (tc/opt-describe {:opt :opt-store-1--money+
                    :title "приходо денег в кассу магазина 1"
                    :opttype opttype-money+
                    :description "простая операция прихода денег."

                    ;; задаем роли, кому разрешена операция
                    :troles-set #{trl-seller-1-key}

                    
                    :owns {:from {:selected-one own-company}
                           :to   {:selected-one own-company}}

                    :directions-set #{azone-black-hole->>-azone-store-1-phisical-money-cash}


                    :rules-for-operatio [(tc/rule-o-boxes-directions-tips
                                          azone-black-hole->>-azone-store-1-phisical-money-cash
                                          #{:money})
                                         ]

                    :money {:selected-list [(tc/row-add-caval currency-KZT)]
                            :direction azone-black-hole->>-azone-store-1-phisical-money-cash}


                    }))



(def opt-store-1--money-
  (tc/opt-describe {:opt :opt-store-1--money-
                    :title "изъятие денег из кассы магазина 1"
                    :opttype opttype-money-
                    :description "простая операция изъятия денег."

                    ;; задаем роли, кому разрешена операция
                    :troles-set #{trl-seller-1-key}

                    
                    :owns {:from {:selected-one own-company}
                           :to   {:selected-one own-company}}

                    :directions-set #{azone-store-1-phisical-money-cash->>-azone-black-hole}


                    :rules-for-operatio [(tc/rule-o-boxes-directions-tips
                                          azone-store-1-phisical-money-cash->>-azone-black-hole
                                          #{:money})
                                         ]

                    :money {:selected-list [(tc/row-add-caval currency-KZT)]
                            :direction azone-store-1-phisical-money-cash->>-azone-black-hole}


                    }))






(def opt-store-1--prod$
  (tc/opt-describe {:opt :opt-store-1--prod$
                    :title "продажа товара в магазине 1"
                    :opttype opttype-prod$
                    :description "Операция продажи товара"
                    :owns {:from {:default own-company}
                           :to {:default own-client-noname
                                :alt-search-in #{:client}}}
                    ;; задать направления
                    :directions-set #{direction->-azone-store-1-phisical-products-for-sale->>-azone-clients
                                      direction->-azone-clients->>-azone-store-1-phisical-money-cash}

                    ;; задаем роли, кому разрешена операция
                    :troles-set #{trl-seller-1-key}

                    ;; задаем правила обмена
                    :swops-set #{swop-1, swop-spec-1}

                    ;; задать правила
                    :rules-for-operatio [(tc/rule-o-boxes-directions-tips
                                          direction->-azone-store-1-phisical-products-for-sale->>-azone-clients
                                          #{:ratio :numeric :bigint})
                                         (tc/rule-o-boxes-directions-tips
                                          direction->-azone-clients->>-azone-store-1-phisical-money-cash
                                          #{:money})
                                         ]


                    ;; функции-валидаторы для проверки перед выполнением
                    ;;:validators  []
                    }))


;; END Operations
;;..................................................................................................





(defn webuser-create-pk []
  (let [webuser-row (ix/webuser-save-for-username {:username "pk"
                                                   :password (cemerick.friend.credentials/hash-bcrypt "paradox")
                                                   :description " особый пользователь "})]

    (ix/webuserwebrole-add-rels webuser-row
                                [webrole-admin webrole-user trl-warehouser-1 trl-seller-1])
    ))


(webuser-create-pk)





(def tag-products (ix/tag-save-const {:constname :countries :tagname "Товары" :parent_id nil}))



(def tag-doc-types (ix/tag-save-const {:constname :doctypes :tagname "Типы документов" :parent_id nil}))

(def tag-news
  (ix/tag-save-const {:constname :news
                      :tagname "Новость"
                      :parent_id (:id tag-doc-types)}))

(def tag-advert
  (ix/tag-save-const {:constname :advert
                      :tagname "Объявление"
                      :parent_id (:id tag-doc-types)}))

(def tag-standart-offer
  (ix/tag-save-const {:constname :standart-offer
                      :tagname "Стандартное"
                      :parent_id (:id tag-advert)}))

(def tag-special-offer
  (ix/tag-save-const {:constname :special-offer
                      :tagname "Спецпредложение"
                      :parent_id (:id tag-advert)}))

(def tag-banner
  (ix/tag-save-const {:constname :banner
                      :tagname "Банер в заголовке"
                      :parent_id (:id tag-doc-types)}))


(def tag-products-groups
  (ix/tag-save-const {:constname :place
                      :tagname "Группа товаров"
                      :parent_id (:id tag-doc-types)}))





;; -----------------------------------------------------------------------------------------------------


(def stext-top-message
  (:keyname
   (ix/stext-save-const {:keyname :TOP_MESSAGE
                         :plantext true
                         :description "Надпись в заголовке"
                         })))


(def stext-pay-key
  (:keyname
   (ix/stext-save-const {:keyname :PAY_AND_DELIVERY
                         :description "Оплата и доставка"
                         })))

#_(def stext-delivery-key
    (:keyname
     (ix/stext-save-const {:keyname :DELIVERY
                           :description "Доставка"
                           })))

(def stext-for-partners-key
  (:keyname
   (ix/stext-save-const {:keyname :FOR-PARTNERS
                         :description "Партнерам"
                         })))



(def stext-company-key
  (:keyname
   (ix/stext-save-const {:keyname :COMPANY
                         :description "О компании"
                         })))
(def stext-contacts-key
  (:keyname
   (ix/stext-save-const {:keyname :CONTACTS
                         :description "Поле контактов на главной странице"
                         })))



(def stext-phone-key
  (:keyname
   (ix/stext-save-const {:keyname :PHONE
                         :plantext true
                         :description "Телефон в заголовке"
                         })))
