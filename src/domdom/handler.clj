(ns domdom.handler
  (:use compojure.core)

  ;;(:use ring.middleware.transit)
  (:use ixinfestor.transit)

  (:require [clojure.java.io :as io]
            [ring.middleware.reload :refer (wrap-reload)] ;; reload temlates
            [ring.middleware.json :as json]
            [ring.middleware.session :as session]
            [ring.middleware.multipart-params :as multipart]

            [compojure.handler :as handler]
            [compojure.route :as route]

            [ixinfestor.core :as ix]
            [ixinfestor.core-handler :as ixch]
            [ixinfestor.core-web :as cw]
            [ixinfestor.core-web-bootstrap :as cwb]

            [tarsonis.core :as ts]
            [tarsonis.core-handler :as tsch]

            [domdom.web :as w]
            [domdom.core :as c]

            [cemerick.friend :as friend]
            (cemerick.friend [workflows :as workflows]
                             [credentials :as creds])

            [clj-http.client :as client]
            )
  )

;;**************************************************************************************************
;;* BEGIN Init web aplication
;;* tag: <init web application>
;;*
;;* description: Инициализация вебприложения
;;*
;;**************************************************************************************************

(defn init []
  (println "INIT SYSTEM")

  ;;  (when ix/development?
  ;;    (do
  ;;      (print "STARTING NREPL -> ")
  ;;(nrepl-server/start-server :port 7809 :handler cider-nrepl-handler)
  ;;      (print "[ok]\n")))


  ;;(print "STARTING SCHEDULLER -> ")
  ;; Some code.....
  ;;(print "[ok]")

  )

;; END Init web aplication
;;..................................................................................................

;;**************************************************************************************************
;;* BEGIN URL ROUTES
;;* tag: <url route system>
;;*
;;* description: Управление вызовами
;;*
;;**************************************************************************************************

(defn ax-form-json [{[ts v] :val :as row}]
  (let [t (keyword ts)]
    (assoc row :tip t
           :val (condp = t
                  :bigint   (ts/ax-cr t (Long/parseLong v))
                  :money    (ts/ax-cr t (bigdec v))
                  :numeric  (ts/ax-cr t (bigdec v))
                  :ratio (let [[n d] v]
                           (ts/ax-cr t (Long/parseLong n) (Long/parseLong d)))
                  (throw (Exception. (str "Немогу сконвертировать json в ax: " row)))))))


(defn make-as-map-for-field [field rows]
  (reduce (fn [a {k field :as row}]
            (assoc a k row))
          {} rows))


(declare import-from-sima-land)

(defroutes app-routes

  (ANY "/" request
       (w/page-main request))

  (ANY "/doc/:id*" request
       (w/page-doc request))

  (GET "/search" request
       (w/page-search request))

  ;; ----------------------------------------------------------------------------------------------

  ;; ----------------------------------------------------------------------------------------------

  (ixch/routes-ix* #{(:keyname c/webrole-user)}
                   {:page-ixcms-main-params
                    {:title "CMS DOM DOM"
                     :navbar-class+ "navbar-inverse"}})

  (ixch/routes-file* #{:user} {:save-file-fn-options {:ws [60 150 300]}})

  ;; ??????
  (ixch/routes-stext* #{:user})
  (ixch/routes-webusers* #{:admin})
  (ixch/routes-tag* #{:user})
  ;; ??????


  (tsch/routes-own* #{:admin :user})
  (tsch/routes-product* #{:admin :user})
  (tsch/routes-ops* {:users-roles-set #{:user}
                     :admin-roles-set #{:admin}})



  ;; ----------------------------------------------------------------------------------------------
  (ANY "/cart/add" {{:keys [id]} :params session :session :as request}
       (let [id (Long/parseLong id)]
         (-> (str "/doc/" id)
             ring.util.response/redirect
             (assoc :session
                    (update-in session [:cart id]
                               (fn [v]
                                 (if (nil? v) 1
                                     (inc v))))))))

  (ANY "/cart/op" {{:keys [id op]} :params session :session :as request}
       (let [id (Long/parseLong id)]
         (-> "/cart"
             ring.util.response/redirect
             (assoc :session
                    (condp = op
                      "inc" (update-in session [:cart id] inc)
                      "dec" (update-in session [:cart id] #(if (= 1 %) % (dec %)))
                      "rm"  (update-in session [:cart] dissoc id)
                      session)))))


  (ANY "/cart" request
       (w/page-cart request))

  (POST "/cart/send" request
        (->  request
             w/page-send
             ring.util.response/response
             (assoc :session (-> request
                                 :session
                                 (assoc :cart {})))
             (ring.util.response/header "Content-Type" "text/html; charset=utf-8")))

  (POST "/tc/import/sima-land" request
        (-> request
            import-from-sima-land
            ring.util.response/response
            cw/error-response-json))

  ;; anonymous
  (GET "/login" [] (hiccup.page/html5 cwb/page-sign-in))
  (GET "/logout" req (friend/logout* (ring.util.response/redirect (str (:context req) "/login"))))


  (route/resources "/")
  (route/not-found "Not Found"))


;; END URL ROUTES
;;..................................................................................................

;;**************************************************************************************************
;;* BEGIN Users and roles autentification
;;* tag: <webusers roles auth>
;;*
;;* description: Аутентификация пользователей
;;*
;;**************************************************************************************************

(defn get-user [username]
  (if-let [webuser-row (ix/webuser-find-by-username username)]
    (->> webuser-row
         ix/webuserwebrole-own-get-rels-set
         (assoc webuser-row :roles))
    false))

;; END Users and roles autentification
;;..................................................................................................
;;**************************************************************************************************
;;* BEGIN Ring middleware
;;* tag: <ring web midleware>
;;*
;;* description: Веб функционал RING
;;*
;;**************************************************************************************************

(def site
  (-> app-routes
      (friend/authenticate {:allow-anon? true
                            :login-uri "/login"
                            :default-landing-uri "/"
                            :unauthorized-handler #(-> (hiccup.page/html5
                                                        [:h2 "You do not have sufficient privileges to access " (:uri %)])
                                                       ring.util.response/response
                                                       (ring.util.response/status 401))
                            :credential-fn (partial creds/bcrypt-credential-fn get-user)
                            :workflows [(workflows/interactive-form)]})
      (session/wrap-session {:cookie-attrs {:max-age (* 3600 24 4)}})
      ring.middleware.keyword-params/wrap-keyword-params


      (json/wrap-json-params   {:bigdecimals? true})
      (wrap-transit-params {:opts {}})

      (wrap-transit-response {:encoding :json, :opts {}})
      (json/wrap-json-response {:keywords? true :bigdecimals? true})

      ))

(def app
  (handler/site site))

;; END Ring middleware
;;..................................................................................................

(def ca-map
  (->> (ts/ca-main-list) (reduce #(assoc %1 (:keyname %2) {:ca_id (:id %2) :tip (:tip %2)}) {})))

(defn assocc-for-ca [row ca-keyname val]
  (-> row
      (merge (ca-map ca-keyname))
      (assoc :val val)))

(defn import-from-sima-land [{{:keys [sid to-tag-id]}:params}]
  (korma.db/transaction
   (let [{:keys [name box_type materials unit photo]
          :as sima-doc} (-> (client/get (str "https://www.sima-land.ru/api/v2/item?&expand=photo&sid=" sid)
                                        {:accept :json :content-type :json :as :json})
                            :body :items first)

          {webdoc-id :id
           :as webdoc-row} (-> {:keyname name
                                :scod sid
                                :web_description (str name
                                                      " Упаковка: " box_type
                                                      " Материал: " materials)
                                :web_meta_description name
                                }

                               (as-> webdoc-row
                                   (condp = unit
                                     "шт"  (assocc-for-ca webdoc-row :standart-bigint [:bigint 1])
                                     ;; "набор" (assocc-for-ca webdoc-row :tip  :ca_id)
                                     ;; "пара" (assocc-for-ca webdoc-row :tip  :ca_id)
                                     ;; "блистер" (assocc-for-ca webdoc-row :tip  :ca_id)
                                     ;; "комплект" (assocc-for-ca webdoc-row :tip  :ca_id)
                                     webdoc-row))
                               ts/webdoc-save)

           {:keys [base_url indexes]} photo
           ]

     (ix/webdoctag-add-tag webdoc-row {:id to-tag-id})

     (doseq [i indexes]
       (let [f "tmp-file.jpg"]
         (println i)
         (clojure.java.io/copy
          (:body (client/get (str base_url "/" i "/1600.jpg") {:as :stream}))
          (java.io.File. f))
         (let[[{path :path :as file-row}] (ix/file-upload-rel-on-o ts/webdoc :webdoc_id webdoc-row
                                                                   {:ws [60 100 150 300]
                                                                    :path-prefix "/image/"}
                                                                   {:filename (str "image/photo-" i ".jpg")
                                                                    :size 0
                                                                    :content_type "image/jpeg"}
                                                                   f)]

           (ix/files-save (assoc file-row :galleria true))

           (when (= i 0)
             (ts/webdoc-save {:id webdoc-id :web_title_image path}))

           )))
     {:result "OK"})))
