(ns domdom.web

  (:use hiccup.core)
  (:use hiccup.page)
  (:use hiccup.form)
  (:use hiccup.element)
  (:use hiccup.util)

  (:require [korma.core :as korc]

            ;;[postal.core :as postal]
            [ixinfestor.core :as ix]
            [ixinfestor.core-web :as cw]
            [ixinfestor.core-web-bootstrap :as cwb]

            [tarsonis.core :as ts]
            [domdom.core :as c]

            [clj-time.format :as timef]
            [clj-time.coerce :as timec]
            [clj-time.local :as timel]

            [postal.core :as postal]
            )
  )

(def news-date-format (java.text.SimpleDateFormat. "dd.MM.yyyy HH:mm"))


(defn webdoc-get-url-path [row]
  (str "/doc" (ix/webdoc-get-url-path row)))



(defn template-main [request body]
  (cwb/template-main
   {:title "Интернетмагазин DOMDOM"
    :header-additions
    (list
     (include-css "/css/shop-homepage.css")
     )}

   (list
    [:nav {:class "navbar navbar-default navbar-fixed-top", :role "navigation"}
     [:div {:class "container"}  "<!-- Brand and toggle get grouped for better mobile display -->"
      [:div {:class "navbar-header"}
       [:button {:type "button", :class "navbar-toggle", :data-toggle "collapse", :data-target "#bs-example-navbar-collapse-1"}
        [:span {:class "sr-only"} "Toggle navigation"]
        [:span {:class "icon-bar"}]
        [:span {:class "icon-bar"}]
        [:span {:class "icon-bar"}] ]
       [:a {:class "navbar-brand", :href "/"}
        [:div {:style "position:relative;top:-10px"}
         [:img {:src "/images/domdomkz.png"
                :class "img-rounded"
                :style "background-color:white;float:left;"
                :height 38 }]
         ]]]  "<!-- Collect the nav links, forms, and other content for toggling -->"
      [:div {:class "collapse navbar-collapse", :id "bs-example-navbar-collapse-1"}
       [:ul {:class "nav navbar-nav"}
        [:li
         [:a {:href "/"} "Главная"] ]
        [:li
         [:a {:href "/#catalog"} "Каталог"] ]
        ;;[:li
        ;; [:a {:href "/#search"} "Поиск"] ]
        [:li
         [:a {:href "/#pay"} "Оплата и доставка"]]
        ;; [:li
        ;; [:a {:href "/#delivery"} "Доставка"]]
        [:li
         [:a {:href "/#for-partners"} "Партнерам"]]
        ;; [:li
        ;; [:a {:href "/#registration"} "Регистрация"]]
        [:li
         [:a {:href "/#news"} "Новости"] ]
        [:li
         [:a {:href "/#company"} "О компании"] ]
        [:li
         [:a {:href "/#contacts"} "Контакты"] ]
        [:li [:a {:href "/#contacts"}
              [:span {:class "glyphicon glyphicon-phone"
                      :style "font-size:1em" :aria-hidden "true"}]
              (:anytext (ix/stext-find c/stext-phone-key))]]]

       [:ul {:class "nav navbar-nav navbar-right"}
        [:li [:a {:href "/cart"}
              [:div {:class "text-danger"}
               [:span {:class "glyphicon glyphicon-shopping-cart"
                       :style "font-size:1em" :aria-hidden "true"}]
               "Корзина ("
               (let [c (get-in request [:session :cart] {})]
                 (-> c keys count)) ")"
               ]]]]

       ]
      "<!-- /.navbar-collapse -->" ]
     "<!-- /.container -->" ]

    "<!-- Page Content -->"
    [:div {:class "container"}

     body

     ]
    "<!-- /.container -->"

    [:div {:class "container"}
     [:hr]  "<!-- Footer -->"
     [:footer
      [:div {:class "row"}
       [:div {:class "col-lg-12"}
        [:p "Copyright © domdom.kz 2015"] ] ] ] ]
    "<!-- /.container -->"
    "<!-- jQuery -->"
    )))


(defn page-main [request]

  (template-main
   request

   (list




    [:div {:class "row"}

     [:img {:src "/images/domdomkz.png" :class "img-rounded col-md-3" }]

     [:div {:class "col-lg-8"}
      [:div
       ;; [:h1 {:class "text-success"} "Интернет магазин DOM DOM"]
       ;;        [:p "Дорогой клиент, мы очень рады вас приветсвовать в нашем интернет магазине.
       ;; Для быстрого нахождения товаров восползуйтесь поиском, либо начните искать интересующий вас товар через каталог."]
       ;;        [:h4 {:class "text-warning"} "Желаем вам приятных покупок!!!"]
       (:anytext (ix/stext-find c/stext-top-message))
       ]

      (form-to
       {:id :search-form}
       [:get "/search"]
       [:div {:class "input-group"}
        [:input {:name :fts :type "text", :class "form-control input-lg", :placeholder "Поиск товара..."}]
        [:span {:class "input-group-btn"}
         [:button {:class "btn btn-success btn-lg", :type "button" :onclick "this.form.submit();"}
          [:span {:class "glyphicon glyphicon-search" :style "font-size:1em" :aria-hidden "true"}]]]]
       )

      "<!-- /input-group -->" ]

     ]

    [:div {:class "row"}

     [:hr {:class "col-md-12" }]



     ]

    [:div {:class "row carousel-holder"}


     ;;(->> 8 range (#(partition-all (/ (count %) 3) %)) (map vector (range)))

     (for [[i rows] (->> (-> ix/webdoc-select*-for-urls
                             (ix/webdoc-pred-by-tags? [c/tag-banner])
                             (korc/order :udate :desc)
                             (ix/com-exec))
                         (#(partition-all (/ (count %) 3) %))
                         (map vector (range)))
           :let [carousel-example-generic-id (str "carousel-example-generic-" i)
                 carousel-example-generic-id-href (str "#" carousel-example-generic-id)]
           ]
       [:div {:class "col-md-4"}
        [:div {:id carousel-example-generic-id,
               :class "carousel slide", :data-ride "carousel"}

         [:ol {:class "carousel-indicators"}
          [:li {:data-target "#carousel-example-generic-0", :data-slide-to "0", :class "active"}]

          (map (fn [i]
                 [:li {:data-target (str "#carousel-example-generic-" i) , :data-slide-to i}])
               (->> rows count (range 1)))
          ]

         [:div {:class "carousel-inner"}

          (let [{:keys [keyname web_top_description web_title_image
                        web_color_0 web_color_1 web_color_2 web_color_3] :as row} (first rows)]
            [:div {:class "item active"}
             [:a {:href (webdoc-get-url-path row)}
              [:img {:class "slide-image", ;;:style "width:300px; height: 300px"
                     :src (str web_title_image "_as_300.png"), :alt "" }]
              [:div {:class "carousel-caption"}
               [:h3 {:style (str "color:" web_color_0 ";text-shadow: 0px 0px 4px " web_color_1 ",0px 0px 7px " web_color_1 )} keyname]
               [:p {:style (str "color:" web_color_2 ";text-shadow: 0px 0px 4px " web_color_3 ",0px 0px 7px " web_color_3 )} web_top_description]]
              ]]
            )

          (map (fn [{:keys [keyname web_top_description web_title_image
                            web_color_0 web_color_1 web_color_2 web_color_3] :as row}]
                 [:div {:class "item"}
                  [:a {:href (webdoc-get-url-path row)}
                   [:img {:class "slide-image", ;;:style "width:300px; height: 300px"
                          :src (str web_title_image "_as_300.png"), :alt ""}]
                   [:div {:class "carousel-caption"}
                    [:h3 {:style (str "color:" web_color_0 ";text-shadow: 0px 0px 4px " web_color_1 ",0px 0px 7px " web_color_1 )} keyname]
                    [:p {:style (str "color:" web_color_2 ";text-shadow: 0px 0px 4px " web_color_3 ",0px 0px 7px " web_color_3 )} web_top_description]]
                   ]])
               (rest rows))
          ]

         [:a {:class "left carousel-control", :href carousel-example-generic-id-href, :data-slide "prev"}
          [:span {:class "glyphicon glyphicon-chevron-left"}] ]
         [:a {:class "right carousel-control", :href carousel-example-generic-id-href, :data-slide "next"}
          [:span {:class "glyphicon glyphicon-chevron-right" }]
          ] ] ]
       )


     ]


    ;; [:div {:class "row"}
    ;;  [:div {:class "col-sm-4 col-lg-4 col-md-4"}
    ;;   [:div {:class "thumbnail"}
    ;;    [:img {:src "http://placehold.it/320x150", :alt ""}]
    ;;    [:div {:class "caption"}
    ;;     [:h4 {:class "pull-right"} "$24.99"]
    ;;     [:h4
    ;;      [:a {:href "#"} "First Product"] ]
    ;;     [:p "See more snippets like this online store item at "
    ;;      [:a {:target "_blank", :href "http://www.bootsnipp.com"} "Bootsnipp - http://bootsnipp.com"] "."] ]
    ;;    [:div {:class "ratings"}
    ;;     [:p {:class "pull-right"} "15 reviews"]
    ;;     [:p
    ;;      [:span {:class "glyphicon glyphicon-star"}]
    ;;      [:span {:class "glyphicon glyphicon-star"}]
    ;;      [:span {:class "glyphicon glyphicon-star"}]
    ;;      [:span {:class "glyphicon glyphicon-star"}]
    ;;      [:span {:class "glyphicon glyphicon-star"}] ] ] ] ]
    ;;  ]

    [:div {:id "catalog" :class "row" }
     [:a {:name "catalog"}]
     (cwb/page-header [:h3 "Каталог"])
     (for [[t w] (ix/tag-get-tree-childs-and-join-webdoc-for-urls
                  [c/tag-products-groups] c/tag-products)
           :when (not (nil? w))]
       [:a {:href (str "/search?group=" (:id t))}
        [:div {:class "col-sm-2 col-md-2"}
         [:div {:class "thumbnail"}
          [:img {:src (str (w :web_title_image) "_as_150.png") :alt "Аватарка"}]
          [:div {:class "caption"}
           [:h5 (w :keyname)]
           [:p [:small (w :web_top_description)]]
           ]]]]
       )
     ]

    [:div {:id "news" :class "row" }
     [:a {:name "news"}]
     (cwb/page-header [:h3 "Новости"])

     (let [{{:keys [news-page] :or {news-page "1"}}:params} request
           page (-> news-page Long/parseLong)
           page-size (-> ix/webdoc-select*
                         (ix/webdoc-pred-by-tags? [c/tag-news])
                         ix/com-count
                         (/ 10)
                         int)
           part 5
           ibegin  (if (< (- page part) 1) 1 (- page part))
           iend    (+ 2 (if (> (+ page part) page-size) page-size (+ page part)))

           ]

       (list
        (for [row (-> ix/webdoc-select*-for-urls
                      (ix/webdoc-pred-by-tags? [c/tag-news])
                      (korc/order :cdate :desc)
                      (korc/offset (* (dec page) 10))
                      (korc/limit 10)
                      (ix/com-exec))]

          [:a {:class "a-news" :href (webdoc-get-url-path row)}
           [:div {:class "panel panel-default"}

            [:div {:class "media panel-body"}
             [:div {:class "media-left"}
              (when-let [i (row :web_title_image)]
                [:img {:class "media-object"
                       :style "width:128px"
                       :src i :alt "Аватарка"}])
              ]
             [:div {:class "media-body"}
              [:h4 {:class "media-heading"}
               [:small ;;{:style "font-size:15px"}
                (.format news-date-format (row :cdate))]
               [:br]
               (row :keyname)]
              (row :web_top_description)
              ]]]]
          )

        [:nav
         [:ul {:class "pagination"}
          [:li {:class ""} [:a {:href (str "/?news-page=1#news") :aria-label "Previous"}
                            [:span {:aria-hidden "true"} "&laquo;"]]]

          (->> (range ibegin iend)
               (map (fn [i]
                      [:li {:class (if (= page i) "active" "")}
                       [:a {:href (str "/?news-page=" i "#news")} i
                        [:span {:class "sr-only"}]]])))

          [:li {:class ""} [:a {:href (str "/?news-page=" (inc page-size) "#news") :aria-label "Previous"}
                            [:span {:aria-hidden "true"} "&raquo;"]]]
          ]
         ]
        ))

     ]

    [:div {:id "pay" :class "row"}
     [:a {:name "pay"}]
     (cwb/page-header [:h3 "Оплата и доставка"])
     (:anytext (ix/stext-find c/stext-pay-key))
     ]

    ;; [:div {:id "delivery" :class "row"}
    ;;  [:a {:name "delivery"}]
    ;;  (cwb/page-header [:h3 "Доставка"])
    ;;  (:anytext (ix/stext-find c/stext-delivery-key))
    ;;  ]

    [:div {:id "for-partners" :class "row"}
     [:a {:name "for-partners"}]
     (cwb/page-header [:h3 "Партнерам"])
     (:anytext (ix/stext-find c/stext-for-partners-key))
     ]

    [:div {:id "company" :class "row"}
     [:a {:name "company"}]
     (cwb/page-header [:h3 "О компании"])
     (:anytext (ix/stext-find c/stext-company-key))
     ]

    [:div {:id "contacts" :class "row"}
     [:a {:name "contacts"}]
     (cwb/page-header [:h3 "Как с нами связаться"])
     (:anytext (ix/stext-find c/stext-contacts-key))
     ]



    )))



(defn page-search [request]
  (let [{{:keys [page page-size fts price-from price-to group order-by]
          :or {page "1"
               page-size "12"
               fts ""
               price-from ""
               price-to ""
               group "0"
               order-by "price-asc"}
          :as params}
         :params} request

         page (Long/parseLong page)
         page-size (Long/parseLong page-size)


         group-row (-> group
                       (#(if (empty? %) "0" %))
                       Long/parseLong
                       (#(if (= % 0)
                           c/tag-products
                           {:id %})))

         groups (ix/tag-get-tree-path-and-childs group-row)

         query* (-> ts/webdoc-select* ;;; SRC
                    (ix/webdoc-pred-search-for-the-child-tree-tags? group-row)
                    (ix/webdoc-pred-by-tags? [c/tag-products-groups] :not)

                    (as-> query
                        (let [fts-query (clojure.string/trim fts)]
                          (if (empty? fts)
                            query
                            (ix/webdoc-pred-search? query fts)))))


         pages (-> query*
                   ix/com-count*
                   (/ page-size)
                   int)
         part 5
         ibegin  (if (< (- page part) 1) 1 (- page part))
         iend    (+ 2 (if (> (+ page part) pages) pages (+ page part)))

         make-page-href (fn [m]
                          (->> (merge params m) seq (reduce (fn [a [k v]] (str a (name k) "=" v "&")) "/search?")))
         ]


    (template-main
     request
     (list
      (form-to
       {:id :search-form}
       [:get "/search"]

       [:div {:class "col-md-3"}
        [:div {:class "list-group"}


         [:fieldset
          [:legend
           [:span {:class "glyphicon glyphicon-search"
                   :style "font-size:1em" :aria-hidden "true"}]
           "Форма поиска товаров"
           ]

          [:div {:class "form-group"}
           [:label {:class "control-label" :for "fts"} "Поиск по словам"]
           [:div {:class ""}
            [:input {:id "fts" :name "fts" :class "form-control"
                     :placeholder "ключевые слова", :type "text"
                     :value fts}]]]

          [:div {:class "form-group"}
           [:label {:class "control-label" :for "price-from"} "Цена от"]
           [:div {:class ""}
            [:input {:id "price-from" :name "price-from" :class "form-control"
                     :placeholder "Цена от (тг.)",
                     :type "number" :min "0"
                     :value price-from}]
            ]]

          [:div {:class "form-group"}
           [:label {:class "control-label" :for "price-to"} "Цена до"]
           [:div {:class ""}
            [:input {:id "price-to" :name "price-to" :class "form-control"
                     :placeholder "Цена до (тг.)",
                     :type "number" :min "0"
                     :value price-to}]
            ]]

          [:input {:type "hidden" :id "group" :name "group" :value (:id group-row)}]
          [:div {:class "form-group"}
           [:label {:class "control-label" :for ""} "По каталогу"]
           [:div {:class ""}

            (letfn [(tree-node-cr [{:keys [id tagname description active? open? const]}]
                      (let [const-css (if const "text-warning" "")]
                        [:li (if active?
                               {:id id :role "presentation" :class "active"}
                               {:id id :role "presentation"})
                         [:a {:href (make-page-href {:group id :page 1})
                              :style "width: 190px"
                              ;;:onclick (or on-click (str "alert(" id ");"))
                              }
                          (if open?
                            [:span {:class (str "glyphicon glyphicon-folder-open " const-css)  :aria-hidden "true"}]
                            [:span {:class (str "glyphicon glyphicon-folder-close " const-css) :aria-hidden "true"}])
                          " "
                          [:span {:class const-css} tagname]
                          ]]))

                    (tree-node-add-childs [tree-node childs]
                      (conj tree-node [:ul {:class "nav nav-pills nav-stacked" :style "margin-left: 5px"} childs]))
                    ]

              [:ul {:class "nav nav-pills nav-stacked"}
               (let [[tree-path childs] groups
                     ;;root-e {:id 0 :tagname "Корень" :open? true}
                     [selected-node & path] tree-path ;;(if (empty? tree-path) [root-e] (conj tree-path root-e))
                     ]
                 (->> childs
                      (map (fn [n] (tree-node-cr n)))
                      (tree-node-add-childs
                       (tree-node-cr (assoc selected-node
                                            :active? true
                                            :open? true
                                            )))
                      ((fn [o]
                         (reduce #(tree-node-add-childs %2 %1)
                                 o (map (fn [n]
                                          (tree-node-cr
                                           (assoc n :open? true)))
                                        path))))
                      ))
               ]
              )
            ]]

          [:input {:class "btn btn-primary" :type "submit" :value  "поиск"}]
          ]]]

       [:div {:class "col-md-9"}
        [:div {:class "row" }
         [:ol {:class "breadcrumb"}
          (let [[[{:keys [tagname]} & path] _] groups]
            (reduce
             (fn [a {:keys [id tagname]}]
               (conj a [:li [:a {:href (str "/search?group=" id)} tagname]]))
             (list [:li {:class "active"} tagname ])
             path))
          ]]]

       [:div {:class "col-md-9"}
        [:div {:class "row"}
         (for [[t w] (ix/tag-get-tree-childs-and-join-webdoc-for-urls
                      [c/tag-products-groups] group-row)
               :when (not (nil? w))]
           [:a {:href "#" :onclick (str "document.getElementById('group').value=" (:id t) "; document.getElementById('search-form').submit();")}
            [:div {:class "col-sm-2 col-md-2"}
             [:div {:class "thumbnail"}
              [:img {:src (str (w :web_title_image) "_as_100.png") :alt "Аватарка"}]
              [:div {:class "caption"}
               [:h5 (w :keyname)]
               [:p [:small (w :web_top_description)]]
               ]]]]
           )
         ]]

       [:div {:class "col-md-9 form-inline panel-heading"}
        [:div {:class "form-group"}
         [:label {:class "control-label" :for "sort"} "Сортировать по:&nbsp;&nbsp;"]

         [:select {:name "order-by" :class "btn btn-default dropdown-toggle form-control"
                   :onchange "document.getElementById('search-form').submit();"}
          (map (fn [[k v]]
                 [:option {:value k :selected (= k order-by)} v])
               [["price-asc" "Цене: по возрастанию"]
                ["price-desc" "Цене: по убыванию"]
                ["by-name" "Наименованию"]])

          ]

         ]
        [:hr]
        ]


       [:div {:class "col-md-9"}
        [:div {:class "row"}
         (for [row (-> query*
                       ts/webdoc-standart-fields*
                       ts/webdoc-swop-join*

                       (as-> query
                           (if (not (empty? price-from))
                             (korc/where query (<= (Long/parseLong price-from) :swop_1.val_money))
                             query))

                       (as-> query
                           (if (not (empty? price-to))
                             (korc/where query (<= :swop_1.val_money (Long/parseLong price-to)))
                             query))

                       (ix/com-pred-page* (dec page) page-size)


                       (as-> query
                           (condp = order-by
                             "price-asc"  (korc/order query :swop_1.val_money :asc)
                             "price-desc" (korc/order query :swop_1.val_money :desc)
                             "by-name" (korc/order query :keyname :asc)
                             (korc/order query :swop_1.val_money :asc)))

                       (ix/com-exec))]

           [:div {:class "col-sm-3 col-lg-3 col-md-3"}
            [:a {:href (webdoc-get-url-path row)}
             [:div {:class "thumbnail" :style "height:280px"}
              (if-let [i (row :web_title_image)]
                [:img {:class "media-object" :style "width:150px,height:150px"
                       :src (str  i "_as_150.png") :alt "Аватарка"}]
                [:div {:align "center"}
                 [:span {:class "glyphicon glyphicon-picture"
                         :style "font-size: 9.5em;color:#ccc" :aria-hidden "true"}]]
                )
              [:div {:class "caption"}
               [:b {:class "pull-right text-warning"} (:swop_1.val_money row)]
               [:h5  (row :keyname)]
               [:p [:small (row :web_top_description)] ]]
              ]]]
           )
         ]

        [:div {:class "row"}
         [:nav
          [:ul {:class "pagination"}
           [:li {:class ""} [:a {:href (make-page-href {:page 1}) :aria-label "Previous"}
                             [:span {:aria-hidden "true"} "&laquo;"]]]

           (->> (range ibegin iend)
                (map (fn [i]
                       [:li {:class (if (= page i) "active" "")}
                        [:a {:href (make-page-href {:page i})} i
                         [:span {:class "sr-only"}]]])))

           [:li {:class ""} [:a {:href (make-page-href {:page (inc pages)}) :aria-label "Previous"}
                             [:span {:aria-hidden "true"} "&raquo;"]]]
           ]
          ]
         ]
        ]
       )))))



(defn page-doc [request]
  (let [{:keys [id cdate
                keyname tag
                web_top_description web_description]
         :as webdoc-row} (-> ts/webdoc-select*
                             ts/webdoc-standart-fields*
                             ts/webdoc-swop-join*
                             (korc/where (= :id (-> request :params :id Long/parseLong)))
                             (korc/with ix/tag)
                             ix/com-exec-1)
         ;;_ (println webdoc-row)

         place-tags-path  (ix/webdoc-row-get-tags-paths-to-root-parent-and-join-webdoc-for-urls
                           webdoc-row [c/tag-products-groups] c/tag-products)

         [[this-tag-point _] & t] place-tags-path

         images (-> webdoc-row
                    (ix/files_rel-select-files-by-* :webdoc_id)
                    ix/file-pred-images*
                    ix/file-pred-galleria?
                    ix/com-exec)
         images? (not (empty? images))

         files (-> webdoc-row
                   (ix/files_rel-select-files-by-* :webdoc_id)
                   (ix/file-pred-images* :not-image)
                   ix/com-exec)
         files? (not (empty? files))



         ]
    (template-main
     request ;;{}
     (cwb/container
      {}

      (list
       [:div {:class "row" }
        [:div {:class "col-md-12"}
         [:a {:name "top"}]

         [:ol {:class "breadcrumb"}
          (if (empty? place-tags-path)
            ;; Если не содержится в дереве товаров то значит не товар
            (list
             [:li [:a {:href "/"} "Главная" ]]
             (letfn [(is-tag? [{id :id}]
                       (contains? (->> webdoc-row :tag (map :id) set ) id))]
               (cond (is-tag? c/tag-news) [:li "новость"]
                     :else "")))
            ;; иначе воспринимать как товар
            (reduce
             (fn [a [{:keys [id tagname]} _]]
               (conj a [:li [:a {:href (str "/search?group=" id)} tagname]]))
             (list [:li {:class "active"} [:a {:href (str "/search?group=" (:id this-tag-point))} (:tagname this-tag-point)]]) (rest place-tags-path)))
          ]

         (cwb/page-header
          [:h2 keyname

           ;;[:small {:style "font-size:0.5em;float:right"} (.format news-date-format cdate)]

           ;;[:span {:class "glyphicon glyphicon-star text-warning"
           ;;        :style "font-size:1em;float:right" :aria-hidden "true"}]
           ])
         ]]

       [:div {:class "row" }
        ;; Фотографии -------------------------------------------------------------------------------
        (when images?

          [:div {:id "gallery" :class "col-md-4" }
           [:a {:name "gallery"}]
           [:div {:class "row" }

            (include-js "/js/jquery.elevatezoom.js")

            [:style "
/*set a border on the images to prevent shifting*/
#gallery_01 img{border:2px solid white;}
/*Change the colour*/
.active img{border:2px solid #333 !important;}
"]

            [:div {:id "gallery"}
             (let [{:keys [path top_description description]} (first images)
                   url-path path]
               [:img {:id "img_01" :src (str url-path "_as_300.png")
                      :data-zoom-image url-path
                      }]
               )

             [:div {:id "gal1"}
              (for [{:keys [path top_description description]}
                    images
                    :let [url-path path]]
                [:a {:href "#", :data-image (str url-path "_as_300.png")
                     :data-zoom-image url-path
                     }
                 [:img {:src (str url-path "_as_60.png")}]
                 ]
                )
              ]]


            (javascript-tag "
//initiate the plugin and pass the id of the div containing gallery images
$('#img_01').elevateZoom(
{
gallery:'gal1',
cursor: 'pointer',
galleryActiveClass: 'active',
imageCrossfade: true,
//loadingIcon: 'http://www.elevateweb.co.uk/spinner.gif'
});

//pass the images to Fancybox
$('#img_01').bind('click', function(e) { var ez = $('#zoom_03').data('elevateZoom');
$.fancybox(ez.getGalleryList());
return false;
});
")

            [:h2 {:class "text-warning" } (:swop_1.val_money webdoc-row)]
            (if-let  [v (get-in request [:session :cart (webdoc-row :id)])]
              [:h4 {:class "text-success" } "Товар уже добавлен в корзину"]
              (form-to {} [:post "/cart/add"]
                       [:input {:name "id" :type "hidden" :value (webdoc-row :id)}]
                       [:input {:type "submit" :class "btn btn-warning btn-lg" :value "Добавить в корзину"}]
                       ))


            ]])

        [:div {:class "col-md-8"}

         [:div {:id "top" :class "row" }
          [:a {:name "top"}]
          web_description]


         ;; файлы ------------------------------------------------------------------------------------

         (when files?
           [:div {:id "files" :class "row" }
            [:a {:name "files"}]
            (cwb/page-header [:h2 "Файлы"])
            (for [i files]
              (let [file-s (str "/file/" (:path i))]
                [:div
                 [:blockquote
                  [:h5 (:top_description i)]
                  (:description i)
                  [:br]
                  [:a {:class "btn btn-success" :href file-s :target "_blank"} "Скачать"]
                  " "[:b (:filename i)]]
                 ]
                ))
            ])


         ]

        ]
       )))))



;; РАбота с корзиной
(defn page-cart [request]
  (let []
    (template-main
     request ;;{}
     (cwb/container
      {}

      (list
       [:div {:class "row" }
        [:div {:class "col-md-12"}
         [:a {:name "top"}]

         [:ol {:class "breadcrumb"}
          [:li [:a {:href "/" } "Главная"]]
          [:li {:class "active"} "корзина"]
          ]

         (cwb/page-header
          [:h1 {:class "text-danger"}
           [:span {:class "glyphicon glyphicon-shopping-cart" :style "font-size:1em" :aria-hidden "true"}]
           "Корзина" ])

         (let [cart (get-in request [:session :cart])]
           (if (not (empty? cart))
             (list
              [:table {:class "table table-striped"}
               [:thead
                [:tr
                 [:th "Товар"]
                 [:th {:style "width:160px"} "Количество"]
                 [:th {:style "width:100px"} "Цена за шт."]
                 [:th {:style "width:120px"} "На сумму"]
                 [:th "Действие"]
                 ]
                ]
               [:tbody
                (->> cart
                     seq
                     (map (fn [[id v]]
                            [(-> ts/webdoc-select*
                                 ts/webdoc-standart-fields*
                                 ts/webdoc-swop-join*
                                 (korc/where (= :id id))
                                 ix/com-exec-1) v]))
                     (reduce (fn [[a s] [{swop_1_val_money :swop_1.val_money
                                          :keys [id keyname web_title_image web_top_description]
                                          :as webdoc-row} v]]
                               [(conj a
                                      [:tr
                                       [:td
                                        [:div {:class "media"}
                                         [:div {:class "media-left"}
                                          (when-let [i web_title_image]
                                            [:img {:class "media-object"  :style "width:60px"
                                                   :src (str i "60") :alt "Аватарка"}])
                                          ]
                                         [:div {:class "media-body"}
                                          [:h4 {:class "media-heading"} keyname]
                                          web_top_description
                                          ]]]
                                       [:td
                                        [:table
                                         [:tr
                                          [:td [:a {:href (str "/cart/op?op=dec&id=" id) :class "btn btn-primary" }
                                                "-"]]
                                          [:td {:style "width:60%;" :align "center"} [:b v]]
                                          [:td [:a {:href (str "/cart/op?op=inc&id=" id) :class "btn btn-success" }
                                                "+"]]]]
                                        ]
                                       [:td {:align "center" } swop_1_val_money]
                                       [:td {:align "center" } (* swop_1_val_money v)]
                                       [:td [:a {:href (str "/cart/op?op=rm&id=" id) :class "btn btn-danger" }
                                             "Удалить"]]
                                       ]) (+ s (* swop_1_val_money v))])
                             [[] 0])
                     ((fn [[items sum]]
                        (-> items
                            (conj [:tr
                                   [:td {:align "right" :colspan "3"} [:h4 "И того на сумму"]]
                                   [:td {:align "center"}  [:h4 {:class "text-warning"} sum]]])
                            list*))))
                ]]

              (form-to {:class "form-horizontal col-sm-12 col-md-10 col-lg-9" }
                       [:post "/cart/send"]

                       [:fieldset
                        [:legend
                         [:span {:class "glyphicon glyphicon-send"
                                 :style "font-size:1em" :aria-hidden "true"}]
                         " Личные данные" ]


                        [:div {:class "form-group"}
                         [:label {:class "col-sm-2 control-label" :for "name"} "Имя"]
                         [:div {:class "col-sm-10"}
                          [:input {:id "name" :name "name"
                                   :class "form-control" :required true
                                   :placeholder "Укажите ваше имя...", :type "text"}]]]

                        [:div {:class "form-group"}
                         [:label {:class "col-sm-2 control-label" :for "email"} "Почта"]
                         [:div {:class "col-sm-10"}
                          [:input {:id "email" :name "email"
                                   :class "form-control" :required true
                                   :placeholder "Укажите адрес электронной почты...", :type "email"}]]]

                        [:div {:class "form-group"}
                         [:label {:class "col-sm-2 control-label" :for "phone"} "Телефон"]
                         [:div {:class "col-sm-10"}
                          [:input {:id "phone" :name "phone"
                                   :class "form-control" :required true
                                   :placeholder "Укажите номер вашего телефона...", :type "tel"}]]]


                        [:div {:class "form-group"}
                         [:label {:class "col-sm-2 control-label" :for "wishes"} "Дополнительно"]
                         [:div {:class "col-sm-10"}
                          [:textarea {:id "wishes" :name "wishes"
                                      :class "form-control" :rows "4"
                                      :placeholder "Прочие пожелания по отдыху...", :type "text"}]]]

                        [:input {:type "submit" :class "btn btn-warning btn-lg" :value "Отправить заказ"}]
                        ]
                       ))
             [:h2 "Корзина пустая..."]
             ))
         ]
        ])))))




;; РАбота с корзиной
(defn page-send [{{:keys [name phone email wishes] :as params} :params :as request}]
  (let [cart (-> (get-in request [:session :cart]) seq)
        zakaz (when-let [cart (get-in request [:session :cart])]
                (->> cart
                     seq
                     (map (fn [[id v]]
                            [(-> ts/webdoc-select*
                                 ts/webdoc-standart-fields*
                                 ts/webdoc-swop-join*
                                 (korc/where (= :id id))
                                 ix/com-exec-1) v]))
                     (reduce (fn [[a s] [{swop_1_val_money :swop_1.val_money
                                          :keys [id scod keyname web_top_description]
                                          :as webdoc-row} v]]
                               [(conj a
                                      [:tr
                                       [:td [:h4 {:class "media-heading"} keyname]
                                        web_top_description ]
                                       [:td scod]
                                       [:td {:align "center" } [:b v]]
                                       [:td {:align "center" } swop_1_val_money]
                                       [:td {:align "center" } (* swop_1_val_money v)]
                                       ]) (+ s (* swop_1_val_money v))])
                             [[] 0])
                     ((fn [[items sum]]
                        [(list
                          [:table {:border 1}
                           [:thead
                            [:tr
                             [:th "Параметр"]
                             [:th "Значение"]
                             ]
                            ]
                           [:tbody
                            [:tr [:td "Имя: "] [:td name]]
                            [:tr [:td "Телефон:"] [:td phone]]
                            [:tr [:td "почта:"] [:td email]]
                            [:tr [:td "пожелания:"] [:td wishes]]
                            ]]

                          [:table {:border 1}
                           [:thead
                            [:tr
                             [:th "Товар"]
                             [:th "Код"]
                             [:th {:style "width:160px"} "Количество"]
                             [:th {:style "width:100px"} "Цена за шт."]
                             [:th {:style "width:120px"} "На сумму"]
                             ]
                            ]
                           [:tbody
                            (-> items
                                (conj [:tr
                                       [:td {:align "right" :colspan "3"} [:h4 "И того на сумму"]]
                                       [:td {:align "center"}  [:h4 {:class "text-warning"} sum]]])
                                list*)]]) sum]))
                     ))

        ]
    (template-main
     (assoc-in request [:session] {}) ;; Отчистка корзины для шаблона
     ;;{}
     (cwb/container
      {}

      (list
       [:div {:class "row" }
        [:div {:class "col-md-12"}
         [:a {:name "top"}]

         [:ol {:class "breadcrumb"}
          [:li [:a {:href "/" } "Главная"]]
          [:li {:class "active"} "результат отправки заказа"]
          ]

         (when zakaz
           (let [[htext sum] zakaz
                 {:keys [code error message]} (postal/send-message {:host "smtp.gmail.com"
                                                                    :user "politrendkz"
                                                                    :pass "paradox_kz"
                                                                    :ssl :yes!!!11}
                                                                   {:from "site@vta.kz"
                                                                    :to ["t34box@gmail.com" "politrendkz@gmail.com"]
                                                                    :cc email
                                                                    :subject "Заказ товара!!!"
                                                                    :body [{:type "text/html;charset=utf-8"
                                                                            :content (html5 htext) }]})]
             (list
              (if (= code 0)
                [:div {:class "alert alert-success" :role "alert"} "Ваше сообщение успешно отправлено"]
                [:div {:class "alert alert-danger" :role "alert"} (str "Ваше сообщение не было отправлено:" message)])

              [:br]
              htext

              )))




         ]])))))










































(comment

  [:div {:class "row"}

   [:div {:class "col-md-3"}
    [:p {:class "lead"}
     [:img {:src "/images/domdomkz.png" :height 240}]]
    [:div {:class "list-group"}
     [:a {:href "#", :class "list-group-item"} "Category 1"]
     [:a {:href "#", :class "list-group-item"} "Category 2"]
     [:a {:href "#", :class "list-group-item"} "Category 3"] ] ]

   [:div {:class "col-md-9"}

    [:div {:class "row carousel-holder"}



     [:div {:class "col-md-5"}
      [:div {:id "carousel-example-generic-0", :class "carousel slide", :data-ride "carousel"}
       [:ol {:class "carousel-indicators"}
        [:li {:data-target "#carousel-example-generic-0", :data-slide-to "0", :class "active"}]
        [:li {:data-target "#carousel-example-generic-0", :data-slide-to "1"}]
        [:li {:data-target "#carousel-example-generic-0", :data-slide-to "2"}] ]
       [:div {:class "carousel-inner"}
        [:div {:class "item active"}
         [:img {:class "slide-image", :src "http://placehold.it/300x200", :alt "" }] ]
        [:div {:class "item"}
         [:img {:class "slide-image", :src "http://placehold.it/300x200", :alt ""}] ]
        [:div {:class "item"}
         [:img {:class "slide-image", :src "http://placehold.it/300x200", :alt ""}] ] ]
       [:a {:class "left carousel-control", :href "#carousel-example-generic-0", :data-slide "prev"}
        [:span {:class "glyphicon glyphicon-chevron-left"}] ]
       [:a {:class "right carousel-control", :href "#carousel-example-generic-0", :data-slide "next"}
        [:span {:class "glyphicon glyphicon-chevron-right"}] ] ] ]

     [:div {:class "col-md-5"}
      [:div {:id "carousel-example-generic-1", :class "carousel slide", :data-ride "carousel"}
       [:ol {:class "carousel-indicators"}
        [:li {:data-target "#carousel-example-generic-1", :data-slide-to "0", :class "active"}]
        [:li {:data-target "#carousel-example-generic-1", :data-slide-to "1"}]
        [:li {:data-target "#carousel-example-generic-1", :data-slide-to "2"}] ]
       [:div {:class "carousel-inner"}
        [:div {:class "item active"}
         [:img {:class "slide-image", :src "http://placehold.it/300x200", :alt "" }] ]
        [:div {:class "item"}
         [:img {:class "slide-image", :src "http://placehold.it/300x200", :alt ""}] ]
        [:div {:class "item"}
         [:img {:class "slide-image", :src "http://placehold.it/300x200", :alt ""}] ] ]
       [:a {:class "left carousel-control", :href "#carousel-example-generic-1", :data-slide "prev"}
        [:span {:class "glyphicon glyphicon-chevron-left"}] ]
       [:a {:class "right carousel-control", :href "#carousel-example-generic-1", :data-slide "next"}
        [:span {:class "glyphicon glyphicon-chevron-right"}] ] ] ]

     ]

    [:div {:class "row"}
     [:div {:class "col-sm-4 col-lg-4 col-md-4"}
      [:div {:class "thumbnail"}
       [:img {:src "http://placehold.it/320x150", :alt ""}]
       [:div {:class "caption"}
        [:h4 {:class "pull-right"} "$24.99"]
        [:h4
         [:a {:href "#"} "First Product"] ]
        [:p "See more snippets like this online store item at "
         [:a {:target "_blank", :href "http://www.bootsnipp.com"} "Bootsnipp - http://bootsnipp.com"] "."] ]
       [:div {:class "ratings"}
        [:p {:class "pull-right"} "15 reviews"]
        [:p
         [:span {:class "glyphicon glyphicon-star"}]
         [:span {:class "glyphicon glyphicon-star"}]
         [:span {:class "glyphicon glyphicon-star"}]
         [:span {:class "glyphicon glyphicon-star"}]
         [:span {:class "glyphicon glyphicon-star"}] ] ] ] ]
     [:div {:class "col-sm-4 col-lg-4 col-md-4"}
      [:div {:class "thumbnail"}
       [:img {:src "http://placehold.it/320x150", :alt ""}]
       [:div {:class "caption"}
        [:h4 {:class "pull-right"} "$64.99"]
        [:h4
         [:a {:href "#"} "Second Product"] ]
        [:p "This is a short description. Lorem ipsum dolor sit amet, consectetur adipiscing elit."] ]
       [:div {:class "ratings"}
        [:p {:class "pull-right"} "12 reviews"]
        [:p
         [:span {:class "glyphicon glyphicon-star"}]
         [:span {:class "glyphicon glyphicon-star"}]
         [:span {:class "glyphicon glyphicon-star"}]
         [:span {:class "glyphicon glyphicon-star"}]
         [:span {:class "glyphicon glyphicon-star-empty"}] ] ] ] ]
     [:div {:class "col-sm-4 col-lg-4 col-md-4"}
      [:div {:class "thumbnail"}
       [:img {:src "http://placehold.it/320x150", :alt ""}]
       [:div {:class "caption"}
        [:h4 {:class "pull-right"} "$74.99"]
        [:h4
         [:a {:href "#"} "Third Product"] ]
        [:p "This is a short description. Lorem ipsum dolor sit amet, consectetur adipiscing elit."] ]
       [:div {:class "ratings"}
        [:p {:class "pull-right"} "31 reviews"]
        [:p
         [:span {:class "glyphicon glyphicon-star"}]
         [:span {:class "glyphicon glyphicon-star"}]
         [:span {:class "glyphicon glyphicon-star"}]
         [:span {:class "glyphicon glyphicon-star"}]
         [:span {:class "glyphicon glyphicon-star-empty"}] ] ] ] ]
     [:div {:class "col-sm-4 col-lg-4 col-md-4"}
      [:div {:class "thumbnail"}
       [:img {:src "http://placehold.it/320x150", :alt ""}]
       [:div {:class "caption"}
        [:h4 {:class "pull-right"} "$84.99"]
        [:h4
         [:a {:href "#"} "Fourth Product"] ]
        [:p "This is a short description. Lorem ipsum dolor sit amet, consectetur adipiscing elit."] ]
       [:div {:class "ratings"}
        [:p {:class "pull-right"} "6 reviews"]
        [:p
         [:span {:class "glyphicon glyphicon-star"}]
         [:span {:class "glyphicon glyphicon-star"}]
         [:span {:class "glyphicon glyphicon-star"}]
         [:span {:class "glyphicon glyphicon-star-empty"}]
         [:span {:class "glyphicon glyphicon-star-empty"}] ] ] ] ]
     [:div {:class "col-sm-4 col-lg-4 col-md-4"}
      [:div {:class "thumbnail"}
       [:img {:src "http://placehold.it/320x150", :alt ""}]
       [:div {:class "caption"}
        [:h4 {:class "pull-right"} "$94.99"]
        [:h4
         [:a {:href "#"} "Fifth Product"] ]
        [:p "This is a short description. Lorem ipsum dolor sit amet, consectetur adipiscing elit."] ]
       [:div {:class "ratings"}
        [:p {:class "pull-right"} "18 reviews"]
        [:p
         [:span {:class "glyphicon glyphicon-star"}]
         [:span {:class "glyphicon glyphicon-star"}]
         [:span {:class "glyphicon glyphicon-star"}]
         [:span {:class "glyphicon glyphicon-star"}]
         [:span {:class "glyphicon glyphicon-star-empty"}] ] ] ] ]
     [:div {:class "col-sm-4 col-lg-4 col-md-4"}
      [:h4
       [:a {:href "#"} "Like this template?"] ]
      [:p "If you like this template, then check out "
       [:a {:target "_blank", :href "http://maxoffsky.com/code-blog/laravel-shop-tutorial-1-building-a-review-system/"} "this tutorial"] " on how to build a working review system for your online store!"]
      [:a {:class "btn btn-primary", :target "_blank", :href "http://maxoffsky.com/code-blog/laravel-shop-tutorial-1-building-a-review-system/"} "View Tutorial"] ] ] ] ]


  )
