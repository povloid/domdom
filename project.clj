(defproject domdom "0.1.1-SNAPSHOT"
  :description "Сайт domdom.kz"
  :url "http://domdom.kz"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.7.0"]
                 [tarsonis "0.1.1"]

                 [clj-http "2.0.0"] ;; Клиент для импорта данных с сималенда
                 [org.apache.httpcomponents/httpclient "4.5"]

                 ;; CLOJURESCRIPT LIBRARIES
                 [org.clojure/clojurescript "1.7.48"]
                 ]


  :jvm-opts ["-Xmx300m" "-server"]
  ;;:jvm-opts ^:replace ["-Xmx700m" "-server"]


  :plugins [[lein-ring "0.9.6"]
            [lein-cljsbuild "1.0.6"]
            [com.cemerick/clojurescript.test "0.3.3"]
            ]

                                        ; Enable the lein hooks for: clean, compile, test, and jar.
  ;;;:hooks [leiningen.cljsbuild]

  ;; все компилируем
  ;; :aot :all
  ;; отключаем исходники
  ;; :omit-source true

  :ring {:handler domdom.handler/app
         :init domdom.handler/init
         :auto-reload? true
         :auto-refresh? false
         :nrepl {:start? true}
         :uberwar-name "domdom.war"
         }


  :clean-targets ^{:protect false} ["resources/public/js/c" "target"]

  :cljsbuild {
              :builds [{:id "dev"
                        :source-paths ["src-cljs" "src-cljs-lib-ixinfestor" "src-cljs-lib-tarsonis"]
                        :compiler {:output-to  "resources/public/js/c/tarsonis/main.js"
                                   :output-dir "resources/public/js/c/tarsonis"
                                   :asset-path "/js/c/tarsonis"
                                   :main tarsonis.main
                                   :optimizations :none
                                   :source-map true}}

                       {:id "prod"
                        :source-paths ["src-cljs"]
                        :compiler {:output-to  "resources/public/js/c/tarsonis/main.js"
                                   :asset-path "/js/c/tarsonis"
                                   :main tarsonis.main
                                   :pretty-print false
                                   :optimizations :advanced ;; мощная оптимизация, но начинаются проблемы с внешними либами
                                   :externs ["externs/jquery-1.9.js" "externs/tarsonis.js"]
                                   }}



                       ]}


  :profiles {:dev
             {:dependencies []

              :plugins []}


             ;; activated automatically during uberjar
             :uberwar {:aot :all
                       :omit-source true
                       }
             ;; :war     {:aot :all
             ;;           ;;:aot [domdom]
             ;;           :omit-source true
             ;;           }
             ;; :uberjar {:aot :all
             ;;           ;;:aot [domdom]
             ;;           :omit-source true
             ;;           }
             ;; :jar     {:aot :all
             ;;           ;;:aot [domdom]
             ;;           :omit-source true
             ;;           }
             }
  )














